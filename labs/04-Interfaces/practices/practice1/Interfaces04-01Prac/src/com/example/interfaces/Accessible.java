/* Copyright © 2017 Oracle and/or its affiliates. All rights reserved. */

package com.example.interfaces;

public interface Accessible {
    
    abstract double verifyDeposit(double amount, int pin);
    abstract double verifyWithdraw(double amount, int pin);
    
}
