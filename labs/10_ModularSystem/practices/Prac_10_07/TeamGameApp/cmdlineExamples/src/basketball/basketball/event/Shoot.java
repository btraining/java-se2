/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package basketball.event;




/**
 *
 * 
 */
public class Shoot extends GameEvent {
    
    public Shoot(){
        
        // No change in ballPoss until next event; Goal or Kickout(save)
        
    }
   
    // This should be more likely when ball is closer to the goal
    // So that passing is rewarded 
    public int getPercentChanceSuccess() {
		// ternary operator below for ballPos
		//int odds = (int)(Math.pow((isHomeTeam?ballPos:reverseBallPos()),2)/200);
		// Below indicate 100% chance if ball at 100 yards!
		int odds = (int)(Math.pow(ballPos,2)/100);
		//System.out.println(ballPos + " : " + odds);
		//System.err.println(this + " : " + ballPos + " : " + odds);
		return odds;
    }    
    public String toString() {
        return "SHOOTS ";
    }
    
    public GameEvent[] getNextEvents() {
        GameEvent theEvent[] = { new Goal()};
        return theEvent;
    }
    public GameEvent[] getNextFailEvent(){
        GameEvent theEvent[] = { new Kickout() };
        return theEvent;
    }
    
    public boolean changePlayer() {
        return false;
    }
    
    public boolean changeTeam() {
        return false;
    }
    
        /**
     * @param ballPos the ballPos to set
     */
    public void setBallPos(int currBallPos) {
        //super.setBallPos(currBallPos);
        super.ballPos = currBallPos;

    }
    
}
