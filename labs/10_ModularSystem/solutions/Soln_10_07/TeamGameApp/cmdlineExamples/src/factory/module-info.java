/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

module factory {
    //requires soccer;
    //requires basketball;
    requires gameapi;
    exports gamefactory; // to competition; Now the transitive requirement in competition for factory will not work.
    //exports gamefactory to competition, main; // Not realistic, how do we know about main?
    uses game.IGameProvider;
    //uses game.IGame;
    //uses game.IPlayer;
    //uses game.ITeam;
}
