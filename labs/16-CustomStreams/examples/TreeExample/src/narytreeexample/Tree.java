

package narytreeexample;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Tree<T> {

    private int size = 0;
    private String indent = "";
    private Node<T> root;


    public Tree(Node<T> root) {
        this.root = root;
        addDetails(root);
        //System.out.println("size: " + size);
    }


    public Node<T> getRoot() {
        return root;
    }


    public int size() {
        return size;
    }


    public ArrayList<Node<T>> getPreOrderList() {
        ArrayList<Node<T>> preOrder = new ArrayList<>();
        preOrder(root, preOrder);
        return preOrder;
    }

    private void preOrder(Node<T> node, ArrayList<Node<T>> preOrder) {
        preOrder.add(node);
        for (Node<T> child : node.getChildren()) {
            preOrder(child, preOrder);
        }
    }

    //private static final String[][] treeIndents = { { " ├─ ", " │  " }, { " └─ ", "    " } };
    private static final String[][] treeIndents = { { " |- ", " |  " }, { " '- ", "    " } };
    
    void print (Node<T> node) {
        // Given there's only one root we can hard-code indent info
        String indent = treeIndents[1][0];
        System.out.println (indent + node.data); 
        printChildren(node, treeIndents[1][1]);
    }
    
    void printChildren (Node<T> node, String indent) {
            
        for( Iterator<Node<T>> iterator = node.getChildren().iterator(); iterator.hasNext(); ) {
            Node<T> child = iterator.next();
            String[] currIndent = treeIndents[iterator.hasNext()? 0 : 1];
            System.out.println (indent + currIndent[0] + child.data); 
            printChildren(child, indent + currIndent[1]);
        }
        
    }
    
    void print5(Node<T> node, String addToIndent) {
        System.out.println(addToIndent + node.data); 
        addToIndent += "   "; 
        for (Node<T> child : node.getChildren()) {
            print5(child, addToIndent);
        }
    }
    
    private void addDetails (Node<T> node) {
        size++;
        for (Node<T> child : node.getChildren()) {
            child.setParent(node);
            addDetails(child);
        } 
    }
    
    public static Tree<String> createSampleData1() {
        Node<String> root = 
        new Node("Owner", new ArrayList(List.of(
            new Node<>("Head of Installation Dept", new ArrayList(List.of(
                new Node<>("Manager", new ArrayList(List.of(
                    new Node<>("Fitter 1"),
                    new Node<>("Fitter 2"),
                    new Node<>("Fitter 3")))), 
                new Node<>("Estimator", new ArrayList(List.of(
                    new Node<>("Scheduler")))),
                new Node<>("Custom Fitter", new ArrayList(List.of(
                    new Node<>("Assistant Fitter"))))))), 
            new Node<>("Head of Sales Dept", new ArrayList(List.of(
                new Node<>("North and East Sales", new ArrayList(List.of(
                    new Node<>("Sales 1")))),
                new Node<>("South Sales")))), 
            new Node<>("Head of Manufacturing Dept", new ArrayList(List.of(
                new Node<>("Head of Design"),
                new Node<>("Manufacture Supervisor", new ArrayList(List.of(
                    new Node<>("Machine Operator 1"),
                    new Node<>("Machine Operator 2"),
                    new Node<>("Machine Operator 3")))),
                    new Node<>("Repairs and Upkeep")))))));

            return new Tree<>(root);
    }
    public static Tree<String> createSampleData() {
 
        Node<String> root = 
            new Node("Owner", new ArrayList(List.of(
                new Node<>("Head of Installation Dept", new ArrayList(List.of(
                    new Node<>("Manager", new ArrayList(List.of(
                        new Node<>("Fitter 1"),
                        new Node<>("Fitter 2"),
                        new Node<>("Fitter 3")))), 
                    new Node<>("Estimator", new ArrayList(List.of(
                        new Node<>("Scheduler")))),
                    new Node<>("Custom Fitter")))), 
                new Node<>("Head of Sales Dept", new ArrayList(List.of(
                    new Node<>("North and East Sales"),
                    new Node<>("South Sales")))), 
                new Node<>("Head of Manufacturing Dept", new ArrayList(List.of(
                    new Node<>("Head of Design"),
                    new Node<>("Manufacture Supervisor", new ArrayList(List.of(
                        new Node<>("Machine Operator 1"),
                        new Node<>("Machine Operator 2"),
                        new Node<>("Machine Operator 3")))),
                        new Node<>("Repairs and Upkeep")))))));
        return new Tree<>(root);
    }
}
