/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package functionalInterface;

//@FunctionalInterface
public interface TestInterface {
    int abstractMethod(int x);
    //int abstractMethod2(int y);
}
