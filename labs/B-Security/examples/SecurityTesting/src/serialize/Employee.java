/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package serialize;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.ObjectStreamField;
import java.io.Serializable;

public class Employee implements Serializable{
    private int salary;
    private transient String fear;
    private static int staticInt;
    /*
    //Solution 2
    private static final ObjectStreamField[] serialPersistentFields = {
        new ObjectStreamField("salary", int.class),
        //new ObjectStreamField("fear", String.class),
    };
    */
    
    public Employee(int salary, String tansientString){
        this.salary = salary;
        this.fear = tansientString;
        staticInt++;
    }
    @Override
    public String toString(){
        return ("Salary:" +salary +", Fear:" +fear +", static:" +staticInt);
    }
    
    /*
    //Solution 3a
    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeObject(this.salary);
        out.writeObject("Fearless copy");
    }
    private void readObject(ObjectInputStream in) throws IOException,ClassNotFoundException {
        this.salary = (int)in.readObject();        
        this.fear = (String)in.readObject();
    }
    */
    
    /*
    //Solution 3b
    private void writeObject(ObjectOutputStream out) throws IOException {
        ObjectOutputStream.PutField fields = out.putFields();
        fields.put("salary", this.salary);
        fields.put("fear", "Fearless Copy");
        out.writeFields();
    }
    private void readObject(ObjectInputStream in) throws IOException,ClassNotFoundException {
        ObjectInputStream.GetField fields = in.readFields();
        this.fear = (String)fields.get("fear", null);
        this.salary = fields.get("salary", 0);
    }
    */
    
    /*
    //Solution 4
    private static class ProxyEmployee implements Serializable{
        private final int rank;       
        public ProxyEmployee(Employee emp){
            rank = (int)(emp.salary/10000 -5);
        }
        private Object readResolve() throws ObjectStreamException {
            return new Employee((rank+5)*10000, "Fearless by Proxy");
        }
    }
    private Object writeReplace() throws ObjectStreamException {
        return new ProxyEmployee(this);
    }
    private void readObject(ObjectInputStream ois) throws InvalidObjectException {
	throw new InvalidObjectException("A proxy was not used!");
    }
    */
    
}
