/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package targetType;

import static java.lang.annotation.ElementType.*;
import java.lang.annotation.Target;

@Target(TYPE)
public @interface AnnotationTypeExample {
    
}
