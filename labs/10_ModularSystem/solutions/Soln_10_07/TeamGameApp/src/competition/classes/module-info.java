/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//
// Note no requirement to soccer or basketball
//

module competition {
    requires transitive factory; // As main calling app needs access to the Factory
    // really like requires AND exports
    //   requires java.logging;
    requires transitive gameapi; // 
    //requires transitive ascii_display;

    // transitive is necessary here nevertheless! Why is this flagged as an issue?
    // Mebbe cos it's not (or shouldn't) provide any part of competitions API
    // and should therefore be accessed directly from main
    //requires transitive display.ascii; // transitive is necessary here nevertheless! Why is this flagged as an issue?
    
    exports tournamentType;
}
