/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package date;

import java.util.Date;

public class TestClass {
    public static void main(String[] args) {
        Date badDate = new Date();
        Example obj = new Example(badDate);

        badDate.setYear(666);
        obj.getDate().setMonth(6);
        System.out.println(obj.getDate());
    }   
}
