package tictactoe;

// TicTacToe.java
// Description: Game engine for TicTacToe game
// Author: Chris Wilcox
// Date: 4/9/2015

// Modified heavily to make work with TicTacToe engine. 
// TODO Little of original remains, but should recode to ensure completely original

import java.util.Random;

public class TicTacToe { 


    
    public enum eStatus {

        IN_PROGRESS(0),
        OPPONENT_WINS(1),
        TIE_GAME(2),
        PLAYER_WINS(3);

        private Integer best;

        eStatus(int best) {
            this.best = best;
        }

        public boolean isWorse(eStatus other) {
            return this.best < other.best;
        }
        public boolean isWorseOrEqual(eStatus other) {
            return this.best <= other.best;
        }
        public boolean isBetter(eStatus other) {
            return this.best > other.best;
        }
        public boolean isBetterOrEqual(eStatus other) {
            return this.best >= other.best;
        }
    }
    
    
    // Instance data

    private char board[][];
    private Random random;
    
    // Note that 4X4 is harder to win (more draws!)
    public int boardSize = 4;
    public int movesTaken = 0;
    
    public char computerPlayer; // TODO encapsulate
    public char opponent;
    
    //private List
    
    
    
    // Constructor
    public TicTacToe(char computerPlayer, char opponent) {
        
        this.computerPlayer = computerPlayer;
        this.opponent = opponent;
        
        // Create random
        random = new Random();

        // Allocate board
        board = new char[boardSize][boardSize];
        
        // Initialize board
        initialize();
    }
    
    // Returns board
    public char[][] data() {
        return getBoard();
    }
    
    // Initialize board
    public void initialize() {
        for (int row = 0; row < boardSize; ++row) {
            for (int col = 0; col < boardSize; ++col) {
                board[row][col] = '-';
            }
        }
        movesTaken = 0;
    }

    // Computer moves
    public void computer(){
        computer(opponent);
    }
    
    // Make random move (for either side)
    public int computer(char side) {
        
        int row = 0;
        int col = 0;

        while (status() == eStatus.IN_PROGRESS) {
        
            row = (int) random.nextInt(boardSize);
            col = (int) random.nextInt(boardSize);

            // Check occupied
            if (getBoard()[row][col] == '-') {
                board[row][col] = side;
                break;
            }
        }
        movesTaken++;
        return row * boardSize + col;
    }
    
    // Make move for either side
    public boolean either(int sqPos, char side) {
        return player(sqPos, side);
    }

    
    // Player moves
    public boolean player(int sqPos) {
        //System.out.println("<<<< Selected move is >>>> " + sqPos);
        return player(sqPos, computerPlayer);
    }    
    public boolean player(int sqPos, char side) {  
        //if (sqPos > 8) System.out.println("BUG>>>>> " +sqPos);
        return player(sqPos/boardSize,sqPos%boardSize, side);
    }
    public boolean player(int row, int col, char side) {
        
        // Check occupied
        if (getBoard()[row][col] == '-') {
            board[row][col] = side;
            movesTaken++;
            return true;
        }
        return false;
    }

    // Game status
    public eStatus status() {

        // Check tie. Checking for board full.
        boolean tieGame = true;
        for (int row = 0; row < boardSize; ++row)
            for (int col = 0; col < boardSize; ++col)
                    if (getBoard()[row][col] == '-')
                        tieGame = false;

        
        // Player wins
        if (runOfThree(computerPlayer)) {
            //currStatus = eStatus.PLAYER_WINS;
            return eStatus.PLAYER_WINS;
        }
        // Computer wins
        else if (runOfThree(opponent)) {
            //currStatus = eStatus.OPPONENT_WINS;
            return eStatus.OPPONENT_WINS;
            
        }
                // Tie vs. in progress game
        if (tieGame)
            return eStatus.TIE_GAME;
        else
        return eStatus.IN_PROGRESS;
    }

        // Run of three?
    private boolean runOfThree2(char c) {

        // Check rows
        for (int row = 0; row <= 3; ++row) {
            if ((getBoard()[row][0] == c) &&
                (getBoard()[row][1] == c) &&
                (getBoard()[row][2] == c))
                return true;
            if ((getBoard()[row][1] == c) &&
                (getBoard()[row][2] == c) &&
                (getBoard()[row][3] == c))
                return true;
        }
        
        // Check columns
        for (int col = 0; col <= 3; ++col) {
            if ((getBoard()[0][col] == c) &&
                (getBoard()[1][col] == c) &&
                (getBoard()[2][col] == c))
                return true;

            if ((getBoard()[1][col] == c) &&
                (getBoard()[2][col] == c) &&
                (getBoard()[3][col] == c))
                return true;
        }
        
        // Check diagonals
        if ((getBoard()[0][0] == c) && 
            (getBoard()[1][1] == c) &&
            (getBoard()[2][2] == c))
            return true;
//        if ((getBoard()[1][0] == c) && 
//            (getBoard()[2][1] == c) &&
//            (getBoard()[3][2] == c))
//            return true;
//        if ((getBoard()[1][1] == c) && 
//            (getBoard()[2][2] == c) &&
//            (getBoard()[3][3] == c))
//            return true;
//        if ((getBoard()[0][1] == c) && 
//            (getBoard()[1][2] == c) &&
//            (getBoard()[2][3] == c))
//            return true;
        
        // Check diagonals
        if ((getBoard()[2][0] == c) && 
            (getBoard()[1][1] == c) &&
            (getBoard()[0][2] == c))
            return true;
//        if ((getBoard()[3][0] == c) && 
//            (getBoard()[2][1] == c) &&
//            (getBoard()[1][2] == c))
//            return true;
//        if ((getBoard()[3][1] == c) && 
//            (getBoard()[2][2] == c) &&
//            (getBoard()[1][3] == c))
//            return true;
//        if ((getBoard()[2][1] == c) && 
//            (getBoard()[1][2] == c) &&
//            (getBoard()[0][3] == c))
//            return true;
        
//        
//        if ((getBoard()[2][0] == c) && 
//            (getBoard()[1][1] == c) &&
//            (getBoard()[0][2] == c))
//            return true;

        return false;
    }

    // Run of three?
    // Rename this or add parameter.
    // How about 3X3 on a 4X4 board
    // Need twice as many checks
    // Or as below but first and last can be missing
    private boolean runOfThree (char c) {

        boolean tempLine = false;
        
        // Check rows
        for (int row = 0; row < boardSize; ++row) {
            // Before checking each line
            tempLine = true;
            for (int col = 0; col < boardSize; col++) {
                if (getBoard()[row][col] != c) {
                    tempLine = false;
                    break;
                }
                if (col == boardSize - 1) return true;  // i.e. It's tested all columns
            }
        }
        //if (tempLine == true) return tempLine;
        
        // Check cols
        for (int col = 0; col < boardSize; ++col) {
            // Before checking each line
            tempLine = true;
            for (int row = 0; row < boardSize; row++) {
                if (getBoard()[row][col] != c) {
                    tempLine = false;
                    break;
                }
                if (row == boardSize - 1) return true;  // i.e. It's tested all rows
            }
        }       
        //if (tempLine == true) return tempLine;
        
        // Check diag 1
        for (int col = 0; col < boardSize; ++col) {
            // Before checking each line
            tempLine = true;
            //for (int row = 0; row < boardSize; row++) {
                if (getBoard()[col][col] != c) {
                    tempLine = false;
                    break;
                }
            if (col == boardSize - 1) return true;  // i.e. It's tested all rows
        }       
        //if (tempLine == true) return tempLine;
        
        // Check diag 2
        for (int col = 0; col < boardSize; ++col) {
            // Before checking each line
            tempLine = true;
            //for (int row = 0; row < boardSize; row++) {
                if (getBoard()[Math.abs(col - (boardSize -1))][col] != c) {     // boardSize - 1 cos 0 indexing
                    tempLine = false;
                    break;
                }
            if (col == boardSize - 1) return true;  // i.e. It's tested all rows
        }       
        //if (tempLine == true) return tempLine;

        return false;
    }
    
    public void showBoard() {
        
        for (char[] currCol: getBoard()) {
            
            for (char currChar: currCol) {
                
                System.out.print(" " + currChar);
            
            }
            System.out.println();
        }
        
    }

    /**
     * @return the board
     */
    public char[][] getBoard() {
        return board;
    }
    
    // Gets the square to move to
    // TODO. Ugly!!! redo
    //
    public int moveIndexToSquareIndex(int moveIndex) {
        int tempMoveIndex = -1;
        for(int i = 0; i < boardSize * boardSize; i++) {
            if (this.board[i/boardSize][i%boardSize] == '-') {
                tempMoveIndex++;
                if(tempMoveIndex == moveIndex ) {
                    return i;
                }
                
            }
            
        }
        return -1; // Not found, not working 
    } 
    
    // TODO. Prob this should use clone?
    public TicTacToe createCopy() {
        TicTacToe ticCopy = new TicTacToe(computerPlayer, opponent);
        // copy values
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                ticCopy.board[i][j] = this.board[i][j];
            }
        }
        return ticCopy;
    }
}

