/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.time.LocalDateTime;

/**
 *
 * 
 */
public interface GameProvider {
    
    Game getGame(Team homeTeam, Team awayTeam, LocalDateTime plusDays);
    
    Player getPlayer(String playerName);
    
    Team getTeam(String teamName, Player[] players);
    
    String getType();
    
}
