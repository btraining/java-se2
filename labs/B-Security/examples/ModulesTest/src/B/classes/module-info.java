/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

module B {
    requires A;
}
