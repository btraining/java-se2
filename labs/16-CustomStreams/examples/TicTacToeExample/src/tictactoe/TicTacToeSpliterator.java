
package tictactoe;

import java.util.Set;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.Collector.Characteristics;
import tictactoe.TicTacToe.eStatus;

/**
 *
 * 
 */


public class TicTacToeSpliterator implements Spliterator<TicTacToe> {

    private final TicTacToe tic;
   
    private eStatus bestSoFar;
    private char currPlayer;
    
    private int[] availableMoves;
    private int current = 0;
    private int end;

    
    public TicTacToeSpliterator(TicTacToe tic) {
        this.bestSoFar = eStatus.IN_PROGRESS;
        this.tic = tic;
        this.currPlayer = tic.computerPlayer;
        availableMoves = createMoveList();
        end = availableMoves.length;

    }    

    // Note how the new Spliterator must use a copy of tic, otherwise we have the problem of
    // each spliterator working with the same data
    private TicTacToeSpliterator(TicTacToe tic, int[] availableMoves, int splitPoint, int end) {
        this.tic = tic.createCopy();
        this.currPlayer = tic.computerPlayer;
        this.availableMoves = availableMoves;
        current = splitPoint;
        this.end = end;
    }   
    
    @Override
    public boolean tryAdvance(Consumer action) { 
        
        if (current < end){
            
            bestSoFar = getNextMoveValue(tic);
            
            // In sequential mode this works!!
//            if (bestSoFar == eStatus.PLAYER_WINS) {
//                current = end;
//                System.out.println("***************************<<<<<< >>>>>>>>***********************");
//            }
            
            action.accept(bestSoFar);
            current++;
            return true;
        }
        return false;
        
    }
    
    private eStatus getNextMoveValue(TicTacToe tic) {
        
        eStatus tempStatus;
        // Make the move then check if it wins or draws immediately
        tic.getBoard()[availableMoves[current]/tic.boardSize][availableMoves[current]%tic.boardSize] = currPlayer;
        tempStatus = tic.status();

        if (tempStatus == eStatus.PLAYER_WINS) {
            tic.getBoard()[availableMoves[current]/tic.boardSize][availableMoves[current]%tic.boardSize] = '-';
            return tempStatus;
        }
        if (tempStatus == eStatus.TIE_GAME) {
            tic.getBoard()[availableMoves[current]/tic.boardSize][availableMoves[current]%tic.boardSize] = '-';
            return tempStatus;
        }

        // If not immediate win or draw use minimax to determine value of move
        tempStatus = MinimaxHelper.getBestMoveWithPruning(tic, currPlayer, eStatus.IN_PROGRESS);

        tic.getBoard()[availableMoves[current]/tic.boardSize][availableMoves[current]%tic.boardSize] = '-';
        return tempStatus; 
        
    } 
    
    // Exact split of one Spliterator per candidate move. Performance very similar
    // to following example of this method.
    @Override
    // Splitting for each possible move
//    public Spliterator<TicTacToe> trySplit() {       
//        int splitPoint = (end - 1);
//        //System.out.println("Splitting " + (end - splitPoint));
//        
//        TicTacToeSpliterator newSplit = new TicTacToeSpliterator(tic, availableMoves, splitPoint, end);
//        
//        this.end = splitPoint;
//        return newSplit;
//    }
    
    //More typical Spliterator where split is in the middle
    public Spliterator<TicTacToe> trySplit() {
        
        //if (end - current < 4 ) return null; // Means splits in four (Better w/o on 4 core machine)
        
        int splitPoint = (end - current + 1)/2 + current;
        //System.out.println("Splitting " + (end - splitPoint));
        
        TicTacToeSpliterator newSplit = new TicTacToeSpliterator(tic, availableMoves, splitPoint, end);
        
        this.end = splitPoint;
        return newSplit;
    }
    
    
    
    @Override
    public long estimateSize() {
        return end - current;
    }

    @Override
    public int characteristics() {
        return 0;
          //Spliterator.ORDERED; // Doesn't fix ordering
    }


    
    private int[] createMoveList() {
        
        int currMove = 0;
        int boardSize = tic.boardSize * tic.boardSize;
        
        int[] moves = new int[boardSize - tic.movesTaken];
        
        for (int i = 0; i < boardSize; i++) {
            if (tic.getBoard()[i/tic.boardSize][i%tic.boardSize] == '-') { 
                moves[currMove] = i;
                currMove++;
            }
            
        }
        
        
        return moves;
    }
 
}