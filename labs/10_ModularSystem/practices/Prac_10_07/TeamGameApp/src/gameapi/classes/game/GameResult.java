/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;



/**
 *
 * 
 */
public interface GameResult {

    /**
     * @return the awayTeam
     */
    Team getAwayTeam();

    int getAwayTeamScore();

    /**
     * @return the homeTeam
     */
    Team getHomeTeam();

    int getHomeTeamScore();

    String getScore();

    // Possibly throw Exception here for game not played or drawn game
    Team getWinner();

    boolean isDrawnGame();
    
}
