
package narytreeexample;

import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.Stack;

/**
 *
 * 
 */

// It doesn't need to specify type. After all, type can only be Node
public class NTreeAsListSpliterator implements Spliterator<Node>  {

    private List treeList;
    private int current, end;

    NTreeAsListSpliterator(Tree t) {
        this.treeList = t.getPreOrderList();
        current = 0;
        end = treeList.size();
    }
    
    NTreeAsListSpliterator(List treeList, int splitPoint, int end) {
        this.treeList = treeList;
        current = splitPoint;
        this.end = end;
    }

    @Override
    public boolean tryAdvance(Consumer action) {
        if (current < end){
            action.accept(treeList.get(current));
            current++;
            return true;
        }
        return false;
    }
 
    @Override
    public Spliterator<Node> trySplit() {
        
        if (end - current < 4) return null; // 8 means splits in four for tree 18 in size as in this example
        
        //System.out.println("Splitting on " + current);
        
        int splitPoint = (end - current + 1)/2 + current;
        //System.out.println(">> Splitting on " + splitPoint);
        NTreeAsListSpliterator newSplit = new NTreeAsListSpliterator(treeList, splitPoint, end);
        
        this.end = splitPoint;
        return newSplit;
    }

//    @Override
//    public Spliterator<Node> trySplit() {
//
//        return null;
//        
//    }

    @Override
    public long estimateSize() {
        return end - current;
    }

    @Override
    public int characteristics() {
        return 0;
    }
    
}