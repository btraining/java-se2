/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;


import java.io.Serializable;


/**
 *
 * 
 */
public interface IGameEvent extends Serializable {

    boolean changePlayer();

    boolean changeTeam();

    /**
     * @return the ballPos
     */
    int getBallPos();

    IGameEvent[] getNextEvents();

    IGameEvent[] getNextFailEvent();

    // This is a default value, but prob this should be abstract
    // Because each Event should do its own stuff to figure out
    // how best to determine odds.
    int getPercentChanceSuccess();

    /**
     * @return the thePlayer
     */
    IPlayer getThePlayer();

    /**
     * @return the theTeam
     */
    ITeam getTheTeam();
    // Eventually maybe IPlayer and Team should not be on the Event. Rather
    // a IPlayer should hold the Event? (8/2016)?

    /**
     * @return the theTime
     */
    double getTheTime();

    // Why is this not flipping back and forward?
    // Also, why are the Magpies even winning?
    // Prob cos half the time it works!!
    //public void changeTeamPossession() {
    //	System.err.println(isHomeTeam);
    //	isHomeTeam = !isHomeTeam; // is "=" required?
    //}
    void reverseBallPos();
    /*    public int reverseBallPos(){
    return soccer.util.Settings.PITCH_LENGTH - this.ballPos;
    }
     */

    /**
     * @param ballPos the ballPos to set
     */
    // TODO This needs to be more complex. At the moment each event just moves the ball a certain amount
    // except for Goal which sets it to 100, and Kickout which sets it to 50, and Shoot which doesn't change it.
    // Note: this is correct! If previous event is Dribble, ballPos represents the position of the ball at the end
    // of the Dribble. The ballPos for Shoot is the ballPos when struck.
    void setBallPos(int ballPos);

    /**
     * @param thePlayer the thePlayer to set
     */
    void setThePlayer(IPlayer thePlayer);

    /**
     * @param theTeam the theTeam to set
     */
    void setTheTeam(ITeam theTeam);

    /**
     * @param theTime the theTime to set
     */
    void setTheTime(double theTime);
    
    boolean isGoal();
    
}
