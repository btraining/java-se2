/* Copyright © 2017 Oracle and/or its affiliates. All rights reserved. */

package com.example.collections;

import java.util.List;
import java.util.Map;
import static java.util.Map.entry;
import java.time.MonthDay;
import java.time.Month;

public class TestClass {
    public static void main(String[] args) {

        //Part 1
        List<Integer> testList = List.of(1,2,4,8,16,32);
        //testList.add(64);
        //testList.set(2, 0);
        
        testList = List.of(0);
        
        testList.forEach(x -> System.out.print(x +", "));
        System.out.println("");
        
        
        //Part 2
        Map<String, MonthDay> testMap =  Map.ofEntries(
                entry("Bologna Day",    MonthDay.of(Month.JANUARY, 16)),
                entry("Opposite Day",   MonthDay.of(Month.FEBRUARY, 13)),
                entry("Panic Day",      MonthDay.of(Month.MARCH, 9)),
                entry("Raymond Day",    MonthDay.of(Month.OCTOBER, 20)),
                entry("Knight Day",     MonthDay.of(Month.DECEMBER, 1))
        );

        testMap.forEach((key,value) -> System.out.println(key +": " +value +", "));
    }   
}
