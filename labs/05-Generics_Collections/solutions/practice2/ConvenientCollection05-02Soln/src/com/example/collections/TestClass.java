/* Copyright © 2017 Oracle and/or its affiliates. All rights reserved. */

package com.example.collections;

import java.util.List;

public class TestClass {
    public static void main(String[] args) {

        //Part 1
        List<Integer> testList = List.of(1,2,4,8,16,32);
        //testList.add(64);
        //testList.set(2, 0);
        
        testList = List.of(0);
        
        testList.forEach(x -> System.out.print(x +", "));
        System.out.println("");
        
        
        
    }   
}
