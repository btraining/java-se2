

package narytreeexample;

import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 *
 * 
 */


public class Test {
    
    public static void main(String[] args) {
        
        // Create a tree
        Tree<String> tree = Tree.createSampleData();

        // Print out entire tree
        tree.print(tree.getRoot());

                Spliterator s0 = new narytreeexample.NTreeSpliterator(tree);
               
                Stream <Node> treeStream = StreamSupport.stream(s0, false);
                
                treeStream
                        .filter(Node::isLeaf)
                        .forEach(System.out::println);

        
    }   
}
