/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.collectors;

/**
 *
 * 
 */
public class LineItem {
    
    private String name;
    private int quantity;
    private double unitPrice;
    
    public LineItem(String theName, int theQuantity, int theUnitPrice) {
        this.name = theName;
        this.quantity = theQuantity;
        this.unitPrice = theUnitPrice;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }
    
     /**
     * @return the price based on amount and unit cost
     */
    public double getTxnTotal() {
        return quantity * unitPrice;
    }   

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the unitPrice
     */
    public double getUnitPrice() {
        return unitPrice;
    }

    /**
     * @param unitPrice the unitPrice to set
     */
    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }
    
    public String toString() {
        return this.name;
    }
    
}
