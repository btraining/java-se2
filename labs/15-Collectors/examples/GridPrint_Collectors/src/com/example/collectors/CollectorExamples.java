package com.example.collectors;

import displayDiagram.Display;
import displayDiagram.DisplayDetail;
import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.flatMapping;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingDouble;


/**
 *
 *
 */
public class CollectorExamples {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        List<ComplexSalesTxn> tList = ComplexSalesTxn.createTxnList();

        Map<String, Map<String, Map<Buyer, Double>>> txnOrderDetails14
                = tList.stream()
                        .collect(groupingBy(ComplexSalesTxn::getCity,
                                groupingBy(ComplexSalesTxn::getSalesPerson,
                                        groupingBy(ComplexSalesTxn::getBuyer,
                                                flatMapping(t -> t.getLineItems().stream(),
                                                        summingDouble(s -> s.getTxnTotal()))))));

        List<List<DisplayDetail>> outputList = new ArrayList();
        txnOrderDetails14.forEach((a, b) -> b.forEach((c, d) -> d.forEach((e, f)
                -> outputList.add(List.of(new DisplayDetail(a),
                        new DisplayDetail(c),
                        new DisplayDetail(e.toString()),
                        new DisplayDetail(f.toString()))))));

        Display.outputTextGrid(outputList);

    }

}
