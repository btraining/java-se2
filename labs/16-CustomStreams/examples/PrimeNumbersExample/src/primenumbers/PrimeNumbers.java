

package primenumbers;

import static java.lang.Math.sqrt;
import java.util.function.LongPredicate;
import java.util.stream.LongStream;
import static java.util.stream.LongStream.rangeClosed;

/**
 *
 * 
 */


public class PrimeNumbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        long startTime = System.nanoTime();

//        long begin = 1_000_000_000_000l;
//        long window = 10_000;
          
        long begin = 100_000;
        long window = 1_000_000;

//        long begin = 9_199_999_999_999_999_999l;    // largest long
//        long window = 20;

        
        long end = begin + window;

//      Various possiblities with outer loop a plain loop and inner loop using a stream

        int howMany = outerPlainLoop(a -> isPrimePlainLoop(a), begin, end);           // Fastest outerPlainLoop example
        //int howMany = outerPlainLoop(a -> isPrimeStreamSequential(a), begin, end);
        //int howMany = outerPlainLoop(a -> isPrimeStreamParallel(a), begin, end);      // Much slower
        
//      Various possiblities with outer loop a stream and inner loop a plain loop 

        //int howMany = outerStreamSequential(a -> isPrimePlainLoop(a), begin, end);      // Approx same as plain loops for both inner and outer
        //int howMany = outerStreamParallel(a -> isPrimePlainLoop(a), begin, end);      // Fastest example of all
//
                
        
        System.out.println("There are " + howMany + " primes " + " between " + begin + " and " + end);
        
        long endTime = System.nanoTime();
        System.out.println("Took "+ ((float)endTime - startTime)/1_000_000_000 + " seconds");        
        
        
    }
    
    // methods with names starting "outer" are different ways of iterating through the
    // range of numbers to be tested
    static int outerPlainLoop(LongPredicate ip, long begin, long end) {
                
        if (begin%2 == 0) begin++;  // Make begin odd
        int howMany = 0;
            for (long n = begin; n < end; n+=2) {

            if (ip.test(n)) {

                howMany++;
            }

        } 
        return howMany;
    }
    
    static int outerStreamSequential(LongPredicate ip, long begin, long end ) {
        
        LongStream theStream = 
        //LongStream.iterate(begin+1, i -> i + 2).limit((end - begin)/2)
        LongStream.rangeClosed(begin, end).filter(n -> n%2!=0)
                .filter(i -> ip.test(i));
        return (int) theStream.count();
    }
    
    static int outerStreamParallel(LongPredicate ip, long begin, long end ) {
        
        LongStream theStream = 
        //LongStream.iterate(begin+1, i -> i + 2).limit((end - begin)/2)
        LongStream.rangeClosed(begin, end).filter(n -> n%2!=0)
                .parallel()
                .filter(i -> ip.test(i));
        return (int) theStream.count();
    }
    
    // methods with names starting "isPrime" are different ways of testing each number 
    // to see if it is prime
    static boolean isPrimePlainLoop (long n) {
   
        for (int i = 3; i <= Math.sqrt(n); i +=2) {
            if (n%i==0) { 
                return false;
            }          
        }
        return true;  
    } 
    
    static boolean isPrimeStreamSequential(long n) {

        return n > 1 && LongStream.iterate(3, i -> i + 2).limit((long)Math.sqrt(n)/2)
            .noneMatch(divisor -> n % divisor == 0);
    }
    
    // Slower than using LongStream.iterate()!!
    static boolean isPrimeStreamSequential02(long n) {
        return n > 1 && rangeClosed(2, (long) sqrt(n))
                .filter((a -> a%2!=0))                      
                .noneMatch(divisor -> n % divisor == 0);
    }
    
    // Very, very slow, almost factor of ten slower. Presumablly parallelization doesn't help that
    // much in finding the first mod = 0, while the overhead of creating many new Streams
    // is prohibitive. Is short-circuiting less efficient when in parallel?
    static boolean isPrimeStreamParallel(long n) {

        // filter helps a little with range here. Also, iterate is much, much slower than range in this case
        //return n > 1 && LongStream.iterate(3, i -> i + 2).limit((long)Math.sqrt(n)/2)
        return n > 1 && rangeClosed(2, (long) sqrt(n)).filter(b -> b%2!=0)
            .parallel()
            .noneMatch(divisor -> n % divisor == 0);
    }
    
}
