/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package basketball.event;



import game.IGameEvent;
import java.io.Serializable;
import game.IPlayer;
import game.ITeam;

/**
 *
 * 
 */

// Not needed due to settings now used on mapper that handles all subtypes
// without annotations necessary

//@JsonIgnoreProperties(ignoreUnknown = true)
//@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
//@JsonSubTypes({
//    @JsonSubTypes.Type(value = Dribble.class, name = "Dribble"),
//    @JsonSubTypes.Type(value = GainPossession.class, name = "GainPossession"),
//    @JsonSubTypes.Type(value = Goal.class, name = "Goal"),
//    @JsonSubTypes.Type(value = Kickoff.class, name = "Kickoff"),
//    @JsonSubTypes.Type(value = Kickout.class, name = "Kickout"),
//    @JsonSubTypes.Type(value = Pass.class, name = "Pass"),
//    @JsonSubTypes.Type(value = ReceivePass.class, name = "ReceivePass"),
//    @JsonSubTypes.Type(value = Shoot.class, name = "Shoot"),
//     }
//)

//@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE);
public abstract class GameEvent implements IGameEvent{
    
     
    
    private ITeam theTeam;
    private IPlayer thePlayer;
    private double theTime;
    int ballPos;
    int AbsBallPos;
    //boolean isHomeTeam = true; // Will start off being home team

    
    public GameEvent(){
        
        // Eventually only for Dribble?

        
    }

    // This is a default value, but prob this should be abstract
    // Because each Event should do its own stuff to figure out
    // how best to determine odds.
    @Override
    public int getPercentChanceSuccess() {
		int odds = 92;
		//System.err.println(this + " : " + ballPos + " : " + odds);
	return odds;  // Default value
    }    

    
    /**
     * @return the theTeam
     */
    @Override
    public ITeam getTheTeam() {
        return theTeam;
    }

    // Eventually maybe IPlayer and Team should not be on the Event. Rather
    // a IPlayer should hold the Event? (8/2016)?



    /**
     * @param theTeam the theTeam to set
     */
    @Override
    public void setTheTeam(ITeam theTeam) {
        this.theTeam = theTeam;
    }

    /**
     * @return the thePlayer
     */
    @Override
    public IPlayer getThePlayer() {
        return thePlayer;
    }

    /**
     * @param thePlayer the thePlayer to set
     */
    @Override
    public void setThePlayer(IPlayer thePlayer) {
        this.thePlayer = thePlayer;
    }

    /**
     * @return the theTime
     */
    @Override
    public double getTheTime() {
        return theTime;
    }

    /**
     * @param theTime the theTime to set
     */
    @Override
    public void setTheTime(double theTime) {
        this.theTime = theTime;
    }
    
    @Override
    public GameEvent[] getNextFailEvent(){
        GameEvent theEvent[] = { new GainPossession() };
        return theEvent;
    }


    /**
     * @return the ballPos
     */
    @Override
    public int getBallPos() {
        return ballPos;
    }

    /**
     * @param ballPos the ballPos to set
     */

    // TODO This needs to be more complex. At the moment each event just moves the ball a certain amount
    // except for Goal which sets it to 100, and Kickout which sets it to 50, and Shoot which doesn't change it.
    // Note: this is correct! If previous event is Dribble, ballPos represents the position of the ball at the end
    // of the Dribble. The ballPos for Shoot is the ballPos when struck.

    @Override
    public void setBallPos(int ballPos) {
        this.ballPos = ballPos + (basketball.util.Settings.PITCH_LENGTH - ballPos)/8;
    }
    
	// Why is this not flipping back and forward?
	// Also, why are the Magpies even winning?
	// Prob cos half the time it works!!
	//public void changeTeamPossession() {
	//	System.err.println(isHomeTeam);
	//	isHomeTeam = !isHomeTeam; // is "=" required?
	//}
    
    @Override
    public void reverseBallPos(){
        this.ballPos = basketball.util.Settings.PITCH_LENGTH - this.ballPos;
    }

/*    public int reverseBallPos(){
        return soccer.util.Settings.PITCH_LENGTH - this.ballPos;
    }
*/
    public boolean isGoal() {
        return false;
    }
    
}
