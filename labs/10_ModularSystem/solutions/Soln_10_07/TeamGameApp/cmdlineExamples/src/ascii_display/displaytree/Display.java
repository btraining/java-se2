/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package displaytree;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * 
 */
public class Display {

    
    // TODO
    // Remove horizontal bar to right of final game
    // Add winner to display?
    // Remove addition lines at top of display
    public static void printLRTree(String theResults) {
        ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
        ArrayList<List> allResults = null;
        try {
            //mapper.writeValue(new File("allGames.json"), allGames);
            TypeReference<List<List<GameDisplayDetails>>> mapType = new TypeReference<List<List<GameDisplayDetails>>>() {
            };

            //allResults = mapper.readValue(new File("allResults.json"), mapType);
            allResults = mapper.readValue(theResults, mapType);

        } catch (IOException ioe) {
            System.out.println("Trouble reading the JSON knockout display details");
            //ioe.printStackTrace();
            System.out.println(ioe.getMessage());
        }

        printLRTourney(allResults);

    }

    private static void printLRTourney(List<List> allResults) {

        // Declare the output array parameters
        int arrayWidth = 0, arrayDepth = 0;

        // Iterate through each round and populate a display List of Lists
        ArrayList<List> allDisplays = new ArrayList();
        List<DisplayLRDetail> detailRound;
        //List<Game> gameRound = null;

        // First populate all the display data              
        int roundNum = 0; // TODO. These should prob just be for loops. Cleaner.

        // ** This determines the vertical distance between the list of games on the left
        int minimumInc = 3;

        for (List<GameDisplayDetails> currRound : allResults) {
            detailRound = new ArrayList();
            DisplayLRDetail gameDisplayDetail = null; // Bit dangerous cos depends on if (gameNum) clause to not fail

            // First row is special as it determines array depth
            int topRight = 0; // for first game in the row
            int verticalPos = 0;

            int gameNum = 0;
            for (GameDisplayDetails currGameResult : currRound) {

                verticalPos += minimumInc;
                //System.out.println("verticalPos " + verticalPos);
                gameDisplayDetail = new DisplayLRDetail(currGameResult, roundNum, verticalPos);
                if (gameNum == 0) {
                    minimumInc *= 2; // double each time gameNum = 0
                }
                // TODO Fix casting
                detailRound.add(gameDisplayDetail);

                // Figure out array depth. Needs to be for first round to accomodate different team names
                if (roundNum == 0) {
                    arrayDepth += gameDisplayDetail.depth;// + 2;// * 3;   // * 2 reqd to leave space for next round games
                }

                gameNum++;
            }

            arrayWidth += gameDisplayDetail.width + gameDisplayDetail.space;

            allDisplays.add(detailRound);
            roundNum++;
            //System.out.println("huh");
        }

        // Create the output char array TODO - Gotta be better way to zero it out!! 
        // Allow for padding so disnae crash
        // Should be exactly right size!!
        char[][] outputArray = new char[arrayDepth][arrayWidth];
        for (int i = 0; i < outputArray.length; i++) {
            //for (int i = 0; i < 49 ; i++) {
            for (int j = 0; j < outputArray[i].length; j++) {
                outputArray[i][j] = ' ';
            }
        }

        //System.out.println(arrayDepth + " : " + arrayWidth);

        // Populate the outputArray
        //int incDepth = 16; // minimum vertical distance between games
        int rowPos = 0;
        int prevRowPos = 0;
        int rowNum = 0;
        for (List currRound : allDisplays) {

            // Print the first row
            int gameNum = 0;
            for (Object currDisplay : currRound) {
                DisplayLRDetail theDisplayDetail = ((DisplayLRDetail) currDisplay);
                rowPos = theDisplayDetail.verticalPos;
                ((DisplayLRDetail) currDisplay).printGameRow(outputArray, rowNum, gameNum, prevRowPos);
                prevRowPos = rowPos;
//int topRight = ;
                //System.out.println(((DisplayLRKnockoutDetail)currDisplay).homeTeam + " : " + ((DisplayLRKnockoutDetail)currDisplay).awayTeam);
                gameNum++;
//break;  
            }

            rowNum++;// += 8;    // This shouldn't be hard-coded. Should be based on space.

        }
        // Check
        //System.out.println("depth " + outputArray.length);
        //System.out.println("length " + outputArray[0].length);

        // Write out the output array
        for (int i = 0; i < outputArray.length; i++) {
            //for (int i = 0; i < 49 ; i++) {

            for (int j = 0; j < outputArray[i].length; j++) {
                //for (int j = 0; j < 99; j++) {

            }

            String toPrint = new String(outputArray[i]);

            System.out.println(toPrint);

        }

    }
    
    public static void outputTextLeagueGrid(String dataGrid) {
        
        // Note - doesn't need access to IDisplayDataItem from gameapi as the data is 
        // retreived from JSON String. Also, as this is text only, the 2D array can 
        // be just DisplayString type
        //
        DisplayString[][] allResults = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            //mapper.writeValue(new File("allGames.json"), allGames);
            //TypeReference<List<List<GameDisplayDetails>>> mapType = new TypeReference<List<List<GameDisplayDetails>>>() {};

            allResults = mapper.readValue(dataGrid, DisplayString[][].class);
            
            //allResults = mapper.readValue(dataGrid, DisplayString[][].class);
            
        } catch (IOException ioe) {
            System.out.println("Trouble reading");
            //ioe.printStackTrace();
            System.out.println(ioe.getMessage());
        }  
        outputTextLeagueGrid(allResults);
        
    }
    
    public static void outputTextLeagueGrid(DisplayString[][] dataGrid) {
        
        // It is simple to iterate through the dataGrid and get each item. 
		// However, this outputTextLeagueGrid() method
        // will output a grid using monospaced text and 
		// therefore needs to calculate how wide to make each column.

        // First find the longest item in any column
        int[] stringLength = new int[dataGrid[0].length];   // stringLength gives a width for each column
        int totalLength = 0;                                // totalLength helps calculate length needed for horizontal line between columns
        
        for (int i = 0; i < dataGrid[0].length; i++){
            
            int currLongest = 0;
            for (DisplayString[] dataGrid1 : dataGrid) {
                int currLength = dataGrid1[i].getDisplayDetail().length();
                if (currLength > currLongest) currLongest = currLength;
            }
            stringLength[i] = currLongest;
            totalLength += currLongest;
        }

        // work out how many extra characters needed for horizontal line between rows
        int numCols = dataGrid[0].length;
        int numExtraCharsPerColum = 3; // One vertical line + space before text and a space after text
        // Create horizontal line of correct length. One extra added to line length for last vertical bar
        String separatorLine = new String(new char[totalLength + (numCols * numExtraCharsPerColum) + 1]).replace("\0", "-"); 
        System.out.println(separatorLine);    
        for (DisplayString[] theRow: dataGrid){

            for (int j = 0; j < theRow.length; j++){
                // How many extra chars required to pad out each item
                int extraChars = stringLength[j] - theRow[j].getDisplayDetail().length();
                // Print out each item in the row    
                System.out.print("| " + theRow[j].getDisplayDetail() + new String(new char[extraChars]).replace("\0", " ") + " ");

            } 
           
            System.out.print("|");  // Print last item in row
            System.out.println();   // Move to next row
            System.out.println(separatorLine);  // Print howizontal separator line
        }
    }
    
    

}
