/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package deprecated;


public class TestAPI {
    
    /**
     * @deprecated
     * This method is deprecated because it's unsafe.
     * Please use newMethod() instead.
    */
    @Deprecated//(since="11", forRemoval=true)
    static void oldMethod(){
        System.out.println("Old");
    }
    
    static void newMethod(){
        System.out.println("New");
    }

}
