/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package customAnnotation;

/*
@ClassPreamble(
    author = "Robin Peck",
    revision = 7,
    revisionDate = "5/15/2019",
    reviewers = {"Ben Ng", "Yu Wong"}
)*/
@ClassPreamble(
        author = "Maureen DeLawn",
        revision = 6,
        revisionDate = "4/15/2019",
        reviewers = {"Ben Ng", "Yu Wong"}
)
public class Gen1List  {

}
