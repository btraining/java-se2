
package tictactoe;


import tictactoe.TicTacToe.eStatus;

/**
 *
 * 
 */


// Currently this is set up to use minimax with pruning

public class MinimaxHelper {
    
    static boolean pruning = true;  // Default is on, but can be turned off
    
    static eStatus getNextMoveValue(TicTacToe tic, char currPlayer, boolean pruningValue) {
        pruning = pruningValue;
        
        eStatus tempStatus = tic.status();

        if (tempStatus == eStatus.PLAYER_WINS) {
            return tempStatus;
        }
        if (tempStatus == eStatus.TIE_GAME) {
            return tempStatus;
        }

        // If not immediate win or draw use minimax to determine value of move
        tempStatus = getBestMoveWithPruning(tic, currPlayer, eStatus.IN_PROGRESS);

        return tempStatus; 
        
    } 
    
    static eStatus getBestMoveWithPruning(TicTacToe currGame, char currPlayer, eStatus currParentValue){
        
        int boardSize = currGame.boardSize;
              
         // Switch sides for each ply as search deepens
         eStatus bestSoFar;// = eStatus.IN_PROGRESS;
        if (currPlayer == currGame.opponent) {
            bestSoFar = eStatus.OPPONENT_WINS;
            currPlayer = currGame.computerPlayer;
        } else {
            bestSoFar = eStatus.PLAYER_WINS;
            currPlayer = currGame.opponent;
        }   

        // TODO. Tidy up.
        
        for (int i = 0; i < (boardSize*boardSize); i++) {
            
            // If clause to ensure only valid moves considered. Bit ugly but works
            if (currGame.getBoard()[i/boardSize][i%boardSize] != '-') continue;
            currGame.getBoard()[i/boardSize][i%boardSize] = currPlayer;
            
            if (currPlayer == currGame.computerPlayer) {    // Looking to maximize

                // Move wins or draws immediately
                if (currGame.status() == eStatus.PLAYER_WINS ) {
                    currGame.getBoard()[i/boardSize][i%boardSize] = '-';    // Undo trial move before returning
                    return eStatus.PLAYER_WINS;
                }  else if (currGame.status() == eStatus.TIE_GAME ) {       // We're at the end! No alternate moves possible
                    currGame.getBoard()[i/boardSize][i%boardSize] = '-';    // Undo trial move before returning
                    return eStatus.TIE_GAME;
                }  

                // If we get to here, not clear yet so continue down the chain of moves
                // recursing through will ensure currPlayer changes again
                // Eventually we must get to a computer win or a tie.
                eStatus currBestReturned = getBestMoveWithPruning(currGame, currPlayer, bestSoFar);
                                                                            
                if (currBestReturned == eStatus.PLAYER_WINS) { 
                    currGame.getBoard()[i/boardSize][i%boardSize] = '-';
                    return currBestReturned;
                }   else if (pruning
                        && currBestReturned.isBetterOrEqual(currParentValue) 
                        && currParentValue != eStatus.IN_PROGRESS) {    // Possible pruning
                    currGame.getBoard()[i/boardSize][i%boardSize] = '-';
                    //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>> max pruning >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                    //System.out.println(currParentValue + " : " + currBestReturned);
                    return currBestReturned; // Return here prunes
                }  else if (currBestReturned == eStatus.TIE_GAME) {
                    currGame.getBoard()[i/boardSize][i%boardSize] = '-';
                    bestSoFar = currBestReturned;
                    continue;
                }  

                currGame.getBoard()[i/boardSize][i%boardSize] = '-';    // Otherwise it's IN_PROGRESS so undo and continue
                
            } else { // currPlayer will be currGame.opponent at this point

                if (currGame.status() == eStatus.OPPONENT_WINS ) {
                    currGame.getBoard()[i/boardSize][i%boardSize] = '-';
                    return eStatus.OPPONENT_WINS;
                    
                }  else if (currGame.status() == eStatus.TIE_GAME ) {
                    currGame.getBoard()[i/boardSize][i%boardSize] = '-';
                    return eStatus.TIE_GAME;
                }
             
                // If we get to here, not clear yet so continue down the chain of moves
                // recursing through will ensure currPlayer changes again
                // Eventually we must get to a computer win or a tie.

                eStatus currBestReturned = getBestMoveWithPruning(currGame, currPlayer, bestSoFar);       
                                                                            
                if (currBestReturned == eStatus.OPPONENT_WINS) { 
                    currGame.getBoard()[i/boardSize][i%boardSize] = '-';
                    return currBestReturned;
                } else if (pruning 
                        && currBestReturned.isWorseOrEqual(currParentValue) 
                        && currParentValue != eStatus.IN_PROGRESS) {   // Possible pruning.
                    currGame.getBoard()[i/boardSize][i%boardSize] = '-';
                    //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>> min pruning >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                    //return eStatus.OPPONENT_WINS; 
                    return currBestReturned;

                }  else if (currBestReturned == eStatus.TIE_GAME) {
                    currGame.getBoard()[i/boardSize][i%boardSize] = '-';
                    bestSoFar = currBestReturned;
                    continue;       // keep looking
                }  
                
                // Found definitive answer for opponent, so now try next move
                currGame.getBoard()[i/boardSize][i%boardSize] = '-';
                
            }

        }
        
       
        return bestSoFar;
    }    
    
}
