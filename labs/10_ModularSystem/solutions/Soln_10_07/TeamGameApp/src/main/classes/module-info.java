/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

module main {
    requires competition; // temporarily transitive to illustrate
                                     // unnamed modules
    //requires display.ascii;
    //exports tournament;              //
    //requires factory;
    //requires soccer; // not reqd cos competition requires soccer transitively
    //requires ascii_display; not reqd cos competition requires ascii_display transitively
    //requires gameapi;  // not reqd cos competition requires soccer transitively which in turn requires gameapi transitively
    //requires jackson.annotations;
    //requires jackson.core;
    //requires jackson.databind;
    requires display.ascii;
}
