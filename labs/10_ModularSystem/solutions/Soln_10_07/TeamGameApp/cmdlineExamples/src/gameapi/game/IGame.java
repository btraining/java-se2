/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;


import java.io.Serializable;
import java.time.LocalDateTime;


/**
 *
 * 
 */
public interface IGame extends IDisplayDataItem, Serializable {

    //
    
    //LocalDateTime myDateTime = LocalDateTime.now();
    //String getGameType();
    
    String getScoreDescriptionString();
    
    // A way to populate Game
    // Using a provider overcomes this so may not be necessary
    void populate(ITeam homeTeam, ITeam awayTeam, LocalDateTime dateTime);
    /**
     * @return the awayTeam
     */
    ITeam getAwayTeam();

    String getDescription(boolean showEvents);

    String getDescription();

    // This returns "Soccer", "Basketball", etc. to identify the game
    // Also was used in IDisplayDataItem to identify how to print on web page
    // Maybe overloading its function?
    String getDetailType();

    // Remainder is displayDetailStuff
    String getDisplayDetail();

    /**
     * @return the events
     */
    IGameEvent[] getEvents();

    // TODO. Perhaps better have the code here, not in constructor of GameResult
    IGameResult getGameResult();

    /**
     * @return the getDetailType
     */
    String getGetDetailType();

    /**
     * @return the homeTeam
     */
    ITeam getHomeTeam();

    int getID();

    /**
     * @return the localDateTime
     */

    LocalDateTime getLocalDateTime();

    // TODO - is there neater way? Ternary works.
    ITeam getOtherTeam(ITeam currTeam);

    String getScore();

    /**
     * @return the theDateTime
     */
    LocalDateTime getTheDateTime();

    boolean isDetailAvailable();

    void playGame();

    /**
     * @param awayTeam the awayTeam to set
     */
    void setAwayTeam(ITeam awayTeam);

    /**
     * @param detailAvailable the detailAvailable to set
     */
    void setDetailAvailable(boolean detailAvailable);

    /**
     * @param events the events to set
     */
    void setEvents(IGameEvent[] events);

    /**
     * @param getDetailType the getDetailType to set
     */
    void setGetDetailType(String detailType);

    /**
     * @param homeTeam the homeTeam to set
     */
    void setHomeTeam(ITeam homeTeam);

    /**
     * @return the id
     */
    //public int getId() {
    //    return id;
    //}
    /**
     * @param id the id to set
     */
    void setId(int id);

    /**
     * @param theDateTime the localDateTime to set
     */
    void setLocalDateTime(LocalDateTime theDateTime);

    /**
     * @param theDateTime the theDateTime to set
     */
    void setTheDateTime(LocalDateTime theDateTime);
    
}
