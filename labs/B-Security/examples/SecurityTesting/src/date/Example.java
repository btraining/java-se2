/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package date;

import java.util.Date;

// Vulnerable version
public class Example {
   private final Date date;
   public Example(Date date) {
      this.date = date;
   }
   public Date getDate() {
      return date;
   }
}

/*
// Safe version
public class Example {
   private final Date date;
   public Example(Date date) {
      this.date = new Date(date.getTime());
   }
   public Date getDate() {
      return (Date)date.clone();
   }
}
*/
