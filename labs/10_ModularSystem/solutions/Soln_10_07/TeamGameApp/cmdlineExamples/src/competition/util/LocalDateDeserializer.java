/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
//import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
//import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * 
 */
public class LocalDateDeserializer extends StdDeserializer<LocalDateTime> {//StdDeserializer

    private static final long serialVersionUID = 1L;

    protected LocalDateDeserializer() {       
        super(LocalDateTime.class);
        //System.out.println("I've instantiated custom deserialization!");
    }


    @Override
    public LocalDateTime deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        //System.out.println("I'm in the deserialize method!");
        return LocalDateTime.parse(jp.readValueAs(String.class),DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

}
