/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package functionalInterface;


public class NewMain {

    public static void main(String[] args) {
        TestInterface testInterface = (x) -> (2*x);
    }  
}
