/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package customAnnotation;

import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Inherited;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//@Repeatable(PreamblesContainer.class)
//@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(TYPE)
public @interface ClassPreamble {
    String author();
    int revision() default 0;
    String revisionDate() default "N/A";
    String[] reviewers();
}


