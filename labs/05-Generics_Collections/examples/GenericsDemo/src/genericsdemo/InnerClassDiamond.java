/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genericsdemo;


public class InnerClassDiamond {
    
    
	public static void main(String args[]) {
		Hello<String> hello = new Hello<>("Java SE 9") {

			@Override
			void hello() {
				System.out.println("Hello " + name);
			}

		};
		hello.hello();

	}

}

abstract class Hello<T> {
	public T name;

	public Hello(T name) {
		this.name = name;
	}

	abstract void hello();
}
    

