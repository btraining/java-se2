/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package soccer.event;


/**
 *
 * 
 */
public class Dribble extends SoccerEvent {
    
    // At the simplest, if successful dribbling moves towards the other teams goal
    public Dribble(){
        
        super();
        
    }

    // This should be more likely when ball is closer to the goal
    // So that passing is rewarded 
    // Dribbling should get harder if lots defenders around. So more difficult closer to Goal.

/*
    public int getPercentChanceSuccess() {
	int odds = (int)(ballPos*ballPos/200);
	//System.out.println(ballPos + " : " + odds);
	return odds;
    }    
*/
    
    public String toString() {
        return "Dribble ";
    }
    
    public SoccerEvent[] getNextEvents() {
        SoccerEvent theEvent[] = { new Shoot(), new Pass()};
        return theEvent;
    }
    
    public boolean changePlayer() {
        return false;
    }
    
    public boolean changeTeam() {
        return false;
    }
    
}
