/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package functionalInterface;

public class TestClass {
    TestInterface lambda = (x) -> (2*x);
}
