/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package soccer.play;

import java.io.Serializable;
import soccer.event.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import game.GameEvent;
import game.Player;
import game.Team;

/**
 *
 * 
 */
public class SoccerTeam implements Team{
    
    private String teamName;
    private Player[] playerArray;
    //@JsonProperty(access = Access.READ_WRITE)
    private int pointsTotal;
    //@JsonProperty(access = Access.READ_WRITE)
    private int goalsTotal;
    private boolean detailAvailable = false;
    private int id = 0;
    private String detailType = "Team";

    // this is temporary to total leagues won for testing.
    public int leaguesWon = 0;


    @Override
    public int compareTo(Object theTeam){
        int returnValue = -1;
        if (this.getPointsTotal()< ((SoccerTeam)theTeam).getPointsTotal()) {
            returnValue = 1;
        } else if (this.getPointsTotal() == ((SoccerTeam)theTeam).getPointsTotal()){
            if (this.getGoalsTotal()< ((SoccerTeam)theTeam).getGoalsTotal()) {
                returnValue = 1;
            } 
        }
        return returnValue;
    }
    
    @Override
    public void incGoalsTotal(int goals){
        this.setGoalsTotal(this.getGoalsTotal() + goals);
    }

    @Override
    public void incPointsTotal(int points){
        this.pointsTotal += points;
    }
    
    public SoccerTeam(String teamName) {
        this.teamName = teamName;
    }
    
    public SoccerTeam(String teamName, Player[] players) {
        this(teamName);
        this.playerArray = players;
    }
    
    public SoccerTeam() {}

    /**
     * @return the teamName
     */
    @Override
    public String getTeamName() {
        return teamName;
    }

    /**
     * @param teamName the teamName to set
     */
    @Override
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    /**
     * @return the playerArray
     */
    @Override
    public Player[] getPlayerArray() {
        return playerArray;
    }

    /**
     * @param playerArray the playerArray to set
     */
    @Override
    public void setPlayerArray(Player[] playerArray) {
        this.playerArray = playerArray;
    }

    /**
     * @return the pointsTotal
     */
    @Override
    public int getPointsTotal() {
        return pointsTotal;
    }

    /**
     * @param pointsTotal the pointsTotal to set
     */
    @Override
    public void setPointsTotal(int pointsTotal) {
        this.pointsTotal = pointsTotal;
    }

    /**
     * @return the goalsTotal
     */
    @Override
    public int getGoalsTotal() {
        return goalsTotal;
    }

    /**
     * @param goalsTotal the goalsTotal to set
     */
    @Override
    public void setGoalsTotal(int goalsTotal) {
        this.goalsTotal = goalsTotal;
    }
    
    @Override
    public String toString(){
        return teamName;
    }
    
    // Remainder is displayDetailStuff
    
    @Override
    public String getDisplayDetail(){
        return teamName;
    }
    @Override
    public boolean isDetailAvailable (){
        return detailAvailable;
    }
    @Override
    public int getID(){
        return id;
    }
    @Override
    public String getDetailType() {
        return detailType;
    }

    /**
     * @param detailAvailable the detailAvailable to set
     */
    @Override
    public void setDetailAvailable(boolean detailAvailable) {
        this.detailAvailable = detailAvailable;
    }

    /**
     * @return the id
     */
    //public int getId() {
    //    return id;
    //}

    /**
     * @param id the id to set
     */
    @Override
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the getDetailType
     */
    @Override
    public String getGetDetailType() {
        return detailType;
    }

    /**
     * @param getDetailType the getDetailType to set
     */
    @Override
    public void setGetDetailType(String detailType) {
        this.detailType = detailType;
    }
    
    
    @Override
    public GameEvent getNextPlayAttempt(GameEvent currEvent){
        
    // Below code shows random selection of attempt
    // Instead this could query the player to determine player's 
    // decision re next attempt.

    // But initially for testing having team decide wot to do is 
    // good. And it cd work that way - but decide based on player attributes.
	// Get current ball position before choosing a new Event

	int ballPos = currEvent.getBallPos();
    GameEvent[] possEvents = currEvent.getNextEvents();


	//System.out.println("** " + currEvent + " - ");


	// Ugly code below to remove Shoot as an option when the ball is not
	// far enough up the pitch.
	// This code should be as an overridden method in special version of Magpies
	// Game should check that returned EventType is valid!!!

		if (teamName.equals("xxxxxxxxx")) {	// Are we a particular team we want to advantage?
			for (GameEvent thisEvent: possEvents ) {
				if ( thisEvent instanceof Shoot) { // Is Shoot valid here?
					if (ballPos < 60 ) {			// Are we a long way from goal? If so remove Shoot
						GameEvent tempEvent = thisEvent;
						ArrayList<GameEvent> eventList = new ArrayList<GameEvent>(Arrays.asList(possEvents));
						eventList.remove(tempEvent);
						possEvents = new SoccerEvent[eventList.size()];
						eventList.toArray(possEvents);

						//System.out.print("SHOOT removed ");
						for (GameEvent theEvent: possEvents) {
							//System.out.print(theEvent + " - ");
						}
						//System.out.println();


					} else if (ballPos < 80) { 
						GameEvent[] newEvents = {new Pass()}; // Currently just for testing
						//possEvents = newEvents;
					} else { // Shoot!!
						GameEvent[] newEvents = {new Shoot()};
						possEvents = newEvents;
						//System.out.print("SHOOT only ");
						for (GameEvent theEvent: possEvents) {
							//System.out.print(theEvent + " - ");
						}
						//System.out.println();
					}
				}		
			 }
		}

		// All events equally likely
        currEvent = possEvents[(int) (Math.random() * (possEvents.length))];
			//System.out.println(currEvent + " - Not a Shoot, cos ball pos is " + ballPos);
			//System.out.println(teamName + " : " + currEvent + " - Not a Shoot, cos ball pos is " + ballPos);
        return currEvent;
		//return new Shoot();
    }
    
    
}


// Just storing for later
       /* 
	// Hack to make shooting less likely
if (teamName.equals("The Magpies")) {
            if (currEvent instanceof Shoot && ballPos < 60) {
//System.out.println("1. It's a Shoot, and ball pos is " + ballPos);
		currEvent = possEvents[(int) (Math.random() * (possEvents.length))];
		if (currEvent instanceof Shoot && ballPos < 60) {
//System.out.println("2. It's a Shoot, and ball pos is " + ballPos);
		    currEvent = possEvents[(int) (Math.random() * (possEvents.length))];
		    if (currEvent instanceof Shoot && ballPos < 60) {
//System.out.println("3. It's a Shoot, and ball pos is " + ballPos);
			currEvent = possEvents[(int) (Math.random() * (possEvents.length))];

		    }
		}
	    } 
}
		if (teamName.equals("The Magpies")) {	// remove Shoot event
			if (ballPos < 70 ) {
				//System.out.println(possEvents.length);
				//ArrayList<GameEvent> eventList = new ArrayList(possEvents);
				ArrayList<GameEvent> eventList = 
				new ArrayList<GameEvent>(Arrays.asList(possEvents));
				eventList.remove(new Shoot());
				SoccerEvent tempEvent = null;
				for (SoccerEvent thisEvent: eventList) {
					if ( thisEvent instanceof Shoot) tempEvent = thisEvent;
				 }
				 eventList.remove(tempEvent);
				possEvents = new SoccerEvent[eventList.size()];
				eventList.toArray(possEvents);
					//System.out.println(possEvents.length);
					//System.out.println(currEvent + " - Not a Shoot, cos ball pos is " + ballPos);
				//for (SoccerEvent thisEvent: possEvents) {
			
					//System.out.print(thisEvent + " - ");

				//}
					//System.out.println();
			}
			// Increase chances of taking a shot - improve this code... ugly!
			// Note should check that Shoot is possible, otherwise will screw things up!
			else if (ballPos>70 && ballPos < 100 && !(currEvent instanceof Shoot)) {
				SoccerEvent[] newEvents = {new Shoot()};
				possEvents = newEvents;
				System.out.println(teamName + " : " + currEvent + " - Should be Shoot, cos ball pos is " + ballPos + " is " + newEvents[0]);
				//return newEvents[0];
			}

		}
*/
