/* Copyright © 2017 Oracle and/or its affiliates. All rights reserved. */

package soccer.play;


public class DisplayString implements IDisplayDataItem {
    
    String displayDetail;
    int id = 0;
    @Override
    public boolean isDetailAvailable () { return false;}
    @Override
    public String getDisplayDetail() {return this.displayDetail;}
    @Override
    public int getID(){return this.id;}
    @Override
    public String getDetailType(){return "String";}
    
    public void setDisplayDetail(String displayDetail){
            this.displayDetail = displayDetail;
    }
    
    public DisplayString(String displayString){
        this.displayDetail = displayString;
    }
}
