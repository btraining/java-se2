/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package override;

public class B extends A {
    //@Override
    void method(){
        System.out.println("B");
    }
}
