/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package xml;

import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;

public class XMLTestMain {

    public static void main(String[] args) {
        try{
            File file = new File("testXMLDocument.xml");
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(file);
            System.out.println(doc.getDocumentElement().getTextContent());
        }
        catch(Exception ex){
            System.out.println(ex);
        }
    }
}
