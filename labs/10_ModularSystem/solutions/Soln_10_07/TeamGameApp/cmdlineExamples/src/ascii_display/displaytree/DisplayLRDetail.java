/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package displaytree;

//import soccer.play.*;


/**
 * 
 * Some things will be hard-coded, but eventually can be in a separate class (that cd read from a file)
 *
 * 
 */
public class DisplayLRDetail {
    
    // TODO there's a bug in ascii_display that causes a problem if the number of rounds
    // is < 2. But perhaps not unreasonable as is it really knockout tourney if only one round?
    
    // Should each object store the details of the above row?
    
    /* Typical display box
        ------------------    
        |  Doves  Ducks  |
        |     2 - 1      |
        ------------------
    */
    
    String homeTeam;
    String awayTeam;
    String winner;
    int width;
    int barWidth;
    int prevBarStart;
    int midpoint;
    
    int topLeft = 0;
    int topRight;
    int lhEdge;     // These may be populated as this is processed, poss even on following row?
    int rhEdge;
    int verticalPos;

    int depth = 7;  // Should be set to number of calls to write to the outputArray - make this dynamic

    String score;
    
    // hard-coded stuff below here
    
    int space = 10;  // Currently only necessary for getting width of array
    int teamPadding = 1;        // TODO!! Fix bug. At the moment this HAS to be 1
    String paddingChar = "#";   // Default is to make padding visible (NOT being used)
    

    
// Used only for the LR version

    DisplayLRDetail(GameDisplayDetails currGame, int rowNum, int verticalPos) { 
        this.homeTeam = currGame.getHomeTeam();
        this.awayTeam = currGame.getAwayTeam();
        
        // Assuming padding before, between, and after
        // Add 2 for vertical box sides
        this.width = homeTeam.length() + awayTeam.length() + (teamPadding * 3) + 2;

                
        this.score = currGame.getScore();
        // Maybe need calculate stuff here depending on which row?   
        
        this.topRight = width; // Little weird cos we need to set it (It's also set in other constructors). TODO improve!!
        
        this.verticalPos = verticalPos;
        // Hard-coding width for horizontal display in LR mode
        this.width = 20; // Could be another constructor setting maximum width
        
        this.topLeft = rowNum + space; // - 1 cos array zero based
        this.topRight = this.topLeft + width;
        this.midpoint = (topRight + topLeft)/2;

    }
    

    String getTeamString() {
        
        return homeTeam + getPaddingString(teamPadding, paddingChar);
        
    }
    
    String getPaddingString(int size, String replaceChar) {
        return new String(new char[size]).replace("\0", replaceChar);
    }
    String getPaddingString(String theString, String replaceChar) {
        return new String(new char[theString.length()]).replace("\0", replaceChar);
    }
    
    // Remaining methods all print 
    void printTopRow() {
        
        printDetail(getPaddingString(width,"-")); 
        printDetail(getPaddingString(space," "));
        
    }
    
    String printTeamRow() {
        
        String teamString = "|" + getPaddingString(teamPadding," ") 
                + homeTeam + getPaddingString(teamPadding," ") 
                + awayTeam;
        int fillOut = width - teamString.length() - 1;
        teamString += getPaddingString(fillOut," ");
        return teamString + "|";
    }
    
    String printScore() {
        
        // This is from previous code - can be improved a lot
        // Maybe make general as needed several times
        // Also != here but == in other place


        
        String scoreString = "|    " + score;
        int fillOut = width - scoreString.length() - 1;
        scoreString += getPaddingString(fillOut," ");
        return scoreString + "|";

    }
    
    void printBottomRow() { // Is this superflous since same as topRow?
        
        printDetail(getPaddingString(width,"-")); // + 2 for the vertical bars
        printDetail(getPaddingString(space," "));
        
    }

    // Following two may be handy if it's necessary to keep track of where cursor is
    String printSpace() {
        return "";//getPaddingString(space,"+");
    }
    
    void printDetail(String theDetail) {
        System.out.print(theDetail);

    }
    
    void printGameRow(char[][] outputArray, int rowNum, int gameNum, int prevRowPos) {
        
        String theGameRow;
        // Note output array index - needs to be calculated  
        // Oh! I shouldn't need to add space as box positions are absolute!!
        
        // If rowNum > 0, print horizontal bar and top vertical bar
        
        //theGameRow = getPaddingString(barWidth,"-");
        
        int rowsPrinted = 0;
        int colPos = 0;
        int rowPos;
    
        rowPos = this.verticalPos;
        
        // ** The multiplier here represents the horizontal distance between columns of games
        colPos = rowNum * 26;    // Was 28
    
        
        // top row
        theGameRow = getPaddingString(width,"-") + printSpace();
        for (int i = 0; i < theGameRow.length(); i ++) {
            outputArray[rowPos + rowsPrinted][i + colPos] = '-';//theGameRow.toCharArray()[i];
        }
        rowsPrinted++;
        
        // teams row
        theGameRow = printTeamRow() + printSpace();
        for (int i = 0; i < theGameRow.length(); i ++) {
            outputArray[rowPos + rowsPrinted][i + colPos] = theGameRow.toCharArray()[i];
        }
        rowsPrinted++;
        
        // Space row plus connection to bar     
        theGameRow = "|" + getPaddingString(width - 2 ," ") + "|--";
        for (int i = 0; i < theGameRow.length(); i ++) {
            outputArray[rowPos + rowsPrinted][i + colPos] = theGameRow.toCharArray()[i];
        }
        
        //Connector from vertical bar
        if (rowNum > 0) {
            theGameRow = "---"; // TODO should be based on column width
            for (int i = 0; i < theGameRow.length(); i++) {
                outputArray[rowPos + rowsPrinted][colPos - i - 1] = theGameRow.toCharArray()[i];
            }
        }
       
        // the downward bar if the gameNum is odd
        if (gameNum%2 != 0) {
            for (int i = 0; i <= this.verticalPos - prevRowPos; i++) {
                
                outputArray[rowPos + rowsPrinted - i][colPos  + width + 2] = '|';
                //System.out.print("!!" + (rowPos + rowsPrinted) + " ++ " + (i + colPos);
            }
        }
        rowsPrinted++;
        
        // score row
        theGameRow = printScore();
        for (int i = 0; i < theGameRow.length(); i ++) {
            outputArray[rowPos + rowsPrinted][i + colPos] = theGameRow.toCharArray()[i];
        }
        rowsPrinted++;
        
        // bottom row
        theGameRow = getPaddingString(width,"-") + printSpace();
        for (int i = 0; i < theGameRow.length(); i ++) {
            outputArray[rowPos + rowsPrinted][i + (colPos)] = theGameRow.toCharArray()[i];
        }
        rowsPrinted++;
        
        //int midpoint = (topRight + topLeft)/2;  // Maybe cd be calculated earlier?
        //outputArray[rowPos + rowsPrinted][midpoint] = '|';
        //rowsPrinted++;
 
        // Test
        //outputArray[8][3] = 'x';
        //outputArray[8][2] = '7';
 
          
    }
   
}
