/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tournamentType;


//import displaytree.DisplayString;

import gamefactory.GameFactory;

import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

//import util.JacksonUtil;
import util.Settings;
import game.Game;
import game.GameEvent;
import game.GameResult;
import game.Player;
import game.Team;


/**
 *
 * 
 */
public class League {

    /**
     * @return the games
     */
    public Game[] getGames() {
        return games;
    }

    /**
     * @return the teams
     */
    public Team[] getTeams() {
        return teams;
    }
    
    static String dirName = "data";
    private String gameType;
    private Game[] games;
    private Team[] teams;

    
    public League(Team[] theTeams, String gameType) {
        if ( theTeams.length < 2 ) {
            System.out.println("Number of teams for a League tournament should be 2 or greater.");
            System.exit(0);
            //throw new Exception("");
        }
        this.teams = theTeams;
        this.gameType = gameType;
    }  
    
//    public League(String dirName, String filename) {
//        getGamesFromJSONFile( dirName,  filename);
//    }  


    // TODO return success/failure?
    public void createGames() {
        int daysBetweenGames = 0;
        
        ArrayList theGames = new ArrayList();
        
        for (Team homeTeam: getTeams()){
            for (Team awayTeam: getTeams()){
               if (homeTeam != awayTeam) {
                   daysBetweenGames += Settings.DAYS_BETWEEN_GAMES;
                   
                   // *** this is where concrete Game is instantiated
                   //theGames.add(new Game(homeTeam, awayTeam, LocalDateTime.now().plusDays(daysBetweenGames)));
                   //theGames.add(GameFactory.getGame(gameType,homeTeam, awayTeam, LocalDateTime.now().plusDays(daysBetweenGames)));
                   theGames.add(GameFactory.getProvider(gameType).getGame(homeTeam, awayTeam, LocalDateTime.now().plusDays(daysBetweenGames)));

               } 
            
            }
        }
        
        
        
        this.games = (Game[]) theGames.toArray(new Game[1]);
    }
    
    // TODO - tidy this method
    // Maybe this should be half of showBestTeam
    // Do we really need theTeams here? Can we get from the Games?
    public void setTeamStats() {

        
        // zero all Team scores and Player scores
        for (Team currTeam: getTeams()){
            currTeam.setGoalsTotal(0);
            currTeam.setPointsTotal(0);
                    // zero all Player scores
            for (Player currPlayer: currTeam.getPlayerArray()){
                currPlayer.setGoalsScored(0);
            }
        }
        
        // Repopulate goalsTotal and pointsTotal on each Team object
        //
        // Note all that is needed from GameResult is:
        // 1. Is the game drawn?
        // 2. Who won the game? (no winner equals drawn? but not good to pass null).
        // 3. What did homeTeam score, what did awayTeam score?
        //
        // Should be possible to get homeTeam and awayTeam from currGame
        
        for (Game currGame: getGames()){
            
            GameResult theResult = currGame.getGameResult(); // 
            //IGameResult theResult = new GameResult(currGame); // Maybe above is better
            
            // Increment pointsTotal on Team
            if (theResult.isDrawnGame()) {
                currGame.getHomeTeam().incPointsTotal(Settings.DRAWN_GAME_POINTS);   // Another way to do it currGame vs. theResult
                theResult.getAwayTeam().incPointsTotal(Settings.DRAWN_GAME_POINTS);
            }

            else {
                theResult.getWinner().incPointsTotal(Settings.WINNER_GAME_POINTS);
            }
            
            // Increment goalsTotal in Team
            theResult.getHomeTeam().incGoalsTotal(theResult.getHomeTeamScore());
            theResult.getAwayTeam().incGoalsTotal(theResult.getAwayTeamScore());
            
        }
    }
    
    // TODO remove theTeams parameter. Need to allow for loading teams from games
    // when loaded from a file
    public void showBestTeam(Team[] theTeams) {
        
	// Note below method puts last first and first last if no scoring at all!
        Arrays.sort(theTeams);
        Team currBestTeam = theTeams[0];  

	// Increment league counter
	//currBestTeam.leaguesWon++;

        //System.out.println("\nTeam Points");       
           
        for (Team currTeam: theTeams){
            //System.out.println(currTeam.getTeamName() + " : " + currTeam.getPointsTotal() + " : " + currTeam.getGoalsTotal());

        }
       // XX Temp commented out - wd be better returning it anyway. 
        //System.out.println("Winner of the League is " + currBestTeam.getTeamName());
        
    }
    
    public String getLeagueAnnouncement(Game[] theGames){
        
        Period thePeriod = Period.between(theGames[0].getTheDateTime().toLocalDate(), 
        theGames[theGames.length - 1].getTheDateTime().toLocalDate());
        
        return "The league is scheduled to run for " +
        thePeriod.getMonths() + " month(s), and " +
        thePeriod.getDays() + " day(s)\n";
    }
    
    
    // Should this zero all players first therefore make getAllPlayers() a util method? TODO
    public void setPlayerStats() {
        for (Game currGame : getGames()) {
            for (GameEvent currEvent : currGame.getEvents()) {
                if (currEvent.isGoal()) {
                    currEvent.getThePlayer().incGoalsScored();
                }
            }
        }

    }
    
    public void showBestPlayersByLeague(Team[] theTeams){
        ArrayList <Player> thePlayers = new ArrayList();
        for (Team currTeam: theTeams){
            thePlayers.addAll(Arrays.asList(currTeam.getPlayerArray()));
        }
        
        Collections.sort(thePlayers, (p1, p2) -> Double.valueOf(p2.getGoalsScored()).compareTo(Double.valueOf(p1.getGoalsScored())));
        
        // How to get the team the player is in? TODO.
        System.out.println("\n\nBest Players in League");
        for (Player currPlayer: thePlayers){
            System.out.println(currPlayer.getPlayerName() + " : " + currPlayer.getGoalsScored());
        }
    }    
    
    public void showBestPlayersByTeam(Team[] theTeams){

        for (Team currTeam: theTeams){
            Arrays.sort(currTeam.getPlayerArray(), (p1, p2) -> Double.valueOf(p2.getGoalsScored()).compareTo(Double.valueOf(p1.getGoalsScored())));

            System.out.println("\n\nBest Players in " + currTeam.getTeamName());
            for (Player currPlayer: currTeam.getPlayerArray()){
                System.out.println(currPlayer.getPlayerName() + " : " + currPlayer.getGoalsScored());
            }
        
        }
     
    }
    
    
//    // TODO - this needs to use simple version with all objects DisplayStrings (otherwise for 
//    // interface type need to use custom Serializer/Deserializer (or use Lists not arrays?)
//    public String getJSONLeagueDataGrid () {
//        
//        DisplayString[][] theGrid = getLeagueDataGrid(this.getGames(), this.getTeams()); // don't use all classes just DisplayString
//        
//        return JacksonUtil.getJSONLeagueDataGrid(theGrid);
//
//    }
//    
//    // This creates a grid of output, all using DisplayString. Use DisplayString for Knockout too?
//    private DisplayString[][] getLeagueDataGrid(Game[] theGames, Team[] theTeams) {
//
//        int numTeams = theTeams.length;
//
//        // Size of grid allow for extra column on the left for list of Teams, and two 
//        // extra columns on right for Points and Goals. Also extra column on top for list of
//        // Teams.
//        DisplayString[][] theGrid = new DisplayString[numTeams + 1][numTeams + 3];
//
//        int colNum = 0;
//        int rowNum = 0;
//
//        // Starting at 0, 0, insert a blank top left corner.
//        theGrid[rowNum][colNum] = new DisplayString("");
//
//        // Do the first row of Teams (headings);
//        for (int i = 0; i < theTeams.length; i++) {
//
//            theTeams[i].setId(i);   // set the Id to the index
//            // ternary expression below determines whether to add the full orginal Team class or a simple 
//            // DisplayString class
//            theGrid[rowNum][colNum + 1] = new DisplayString(theTeams[i].getTeamName());
//            //theGrid[rowNum][colNum + 1] = theTeams[i];
//            colNum++;
//        }
//
//        // Add Points and Games columns to the first row (headings)
//        theGrid[rowNum][colNum + 1] = new DisplayString("Points");
//        // Getting "Goals" vs "Baskets" from Game (same on all IGames - another approach?)
//        theGrid[rowNum][colNum + 2] = new DisplayString(theGames[0].getScoreDescriptionString());
//
//        // Add each row of Games for each home team (note all Team IDs will be set by previous for loop
//        // Also note rowNum = i + 1; therefore starting on second row.
//        for (int i = 0; i < theTeams.length; i++) {
//            rowNum = i + 1;
//            
//            // Add the home Team to the first column of the current row
//            colNum = 0;
//            Team currHomeTeam = theTeams[i];
//            //theGrid[rowNum][colNum] = currHomeTeam;
//            
//            theGrid[rowNum][colNum] = new DisplayString(currHomeTeam.getTeamName());
//            //theGrid[rowNum][colNum] = theTeams[i];
//
//
//            // Inner loop through all away teams on current row to add Games
//            for (Team currAwayTeam : theTeams) {
//                colNum++;   // Could also use traditional for loop here
//                if (currHomeTeam != currAwayTeam) {
//                    for (Game theGame : theGames) {
//                        if (theGame.getHomeTeam().getTeamName().equals(currHomeTeam.getTeamName()) 
//                                && theGame.getAwayTeam().getTeamName().equals(currAwayTeam.getTeamName())) {
//                            //theGrid[rowNum][colNum] = theGame;
//                            //theGrid[rowNum][colNum] = useOriginalClass?theGame:new DisplayString(theGame.getScore());
//                            //theGrid[rowNum][colNum] = theGame;
//                            //System.out.println("Setting the score!");
//                            theGrid[rowNum][colNum] = new DisplayString(theGame.getScore());
//                            break;
//                        }
//                    }
//                } else {
//                    theGrid[rowNum][colNum] = new DisplayString(" X ");    // Mark with X as team doesn't play itself
//                }
//            }
//
//            // Add last two columns to current row (team points and goals)
//            theGrid[rowNum][colNum + 1] = new DisplayString(Integer.toString(currHomeTeam.getPointsTotal()));
//            theGrid[rowNum][colNum + 2] = new DisplayString(Integer.toString(currHomeTeam.getGoalsTotal()));
//        }
//        return theGrid;
//    }
//    
    // Should be in ascii_display
//    private void outputTextLeagueGrid(DisplayString[][] dataGrid) {
//        
//        // It is simple to iterate through the dataGrid and get each item. 
//        // However, this outputTextLeagueGrid() method
//        // will output a grid using monospaced text and 
//	// therefore needs to calculate how wide to make each column.
//
//        // First find the longest item in any column
//        int[] stringLength = new int[dataGrid[0].length];   // stringLength gives a width for each column
//        int totalLength = 0;                                // totalLength helps calculate length needed for horizontal line between columns
//        
//        for (int i = 0; i < dataGrid[0].length; i++){
//            
//            int currLongest = 0;
//            for (DisplayString[] dataGrid1 : dataGrid) {
//                int currLength = dataGrid1[i].getDisplayDetail().length();
//                if (currLength > currLongest) currLongest = currLength;
//            }
//            stringLength[i] = currLongest;
//            totalLength += currLongest;
//        }
//
//        // work out how many extra characters needed for horizontal line between rows
//        int numCols = dataGrid[0].length;
//        int numExtraCharsPerColum = 3; // One vertical line + space before text and a space after text
//        // Create horizontal line of correct length. One extra added to line length for last vertical bar
//        String separatorLine = new String(new char[totalLength + (numCols * numExtraCharsPerColum) + 1]).replace("\0", "-"); 
//        System.out.println(separatorLine);    
//        for (DisplayString[] theRow: dataGrid){
//
//            for (int j = 0; j < theRow.length; j++){
//                // How many extra chars required to pad out each item
//                int extraChars = stringLength[j] - theRow[j].getDisplayDetail().length();
//                // Print out each item in the row    
//                System.out.print("| " + theRow[j].getDisplayDetail() + new String(new char[extraChars]).replace("\0", " ") + " ");
//
//            } 
//           
//            System.out.print("|");  // Print last item in row
//            System.out.println();   // Move to next row
//            System.out.println(separatorLine);  // Print howizontal separator line
//        }
//    }
        
//    public void simpleDisplay(IDisplayDataItem[][] dataGrid) {
//    // Iterate through everything using nested enhanced for loop     
//    for (IDisplayDataItem[] theRow: dataGrid){
//
//            for (IDisplayDataItem theItem: theRow){
//
//                    System.out.print(theItem.getDisplayDetail() + " : ");
//
//            } 
//            System.out.println();
//        }
//    }
    
//    public void saveGamesToJSONFile(String dirName, String filename ) {
//
//        JacksonUtil.saveToJSONFile(dirName, filename, this.getGames());
//
//    }
//    
//    public void getGamesFromJSONFile(String dirName, String filename) {
//                //System.out.println("Reading league games from JSON");
//                
//        Game[] allGames = JacksonUtil.getGamesFromJSONFile(dirName, filename);
//        this.games = allGames;
//        this.teams = getTeamsFromGames(allGames);
//        
//    }


    private Team[] getTeamsFromGames(Game[] theGames) {

                
        int numTeams = (1 + (int)Math.sqrt(1 + 4*theGames.length))/2; // Quadratic equation to get num teams from num games
        Team[] theTeams = new Team[numTeams];

        for (int i = 0; i < numTeams; i++) {
            theTeams[i] = theGames[i].getAwayTeam();
        }

        this.showBestTeam(theTeams); // Need to order before returning as this isn't saved
        return theTeams;
    }
    
    public void createAndPlayAllGames() {
        this.createGames();
        for (Game currGame : this.getGames()) {
            currGame.playGame();

        }
        this.setTeamStats();
        this.setPlayerStats();
        this.showBestTeam(getTeams()); // Sets team order in the GRID

    }
    
    
}


