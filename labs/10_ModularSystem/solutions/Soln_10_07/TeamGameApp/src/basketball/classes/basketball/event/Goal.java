/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package basketball.event;




/**
 *
 * 
 */
public class Goal extends BasketBallEvent  {
    
    public Goal(){
       
    }
    public String toString() {
        return "GOAL! ";
    }
    
    // Hack? Fail not relevant here as Goal is a different type
    // of Event - it's not an attempt at something.
    // Perhaps the thing to do is combine Goal and Kickoff
    // As one Event. Makes sense. 
    public BasketBallEvent[] getNextFailEvent(){
        BasketBallEvent theEvent[] = {new Kickoff()};
        return theEvent;
    }
    public BasketBallEvent[] getNextEvents() {
        BasketBallEvent theEvent[] = {new Kickoff()};
        return theEvent;
    }
    
    public boolean changePlayer() {
        return false;
    }
    
    public boolean changeTeam() {
        return false;
    }
    
    
    // Little bit of a hack maybe as ballPos not used.
    public void setBallPos(int ballPos) {
       //super.setBallPos(100);
        super.ballPos = 100;
    }
    
    public boolean isGoal() {
        return true;
    }
     
}
