/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package serialize;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class TestClass {

    public static void main(String[] args) {
        Employee emp = new Employee(80_000, "Spiders");
        String fileName = "test.txt";

        try {
            //Serialize
            FileOutputStream fos = new FileOutputStream(fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(emp);
            fos.close();
            oos.close();
            
            //Deserialize
            FileInputStream fis = new FileInputStream(fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Employee deserializedEmployee = (Employee)ois.readObject();
            fis.close();
            ois.close();
            System.out.println(deserializedEmployee);
        } 
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
} 