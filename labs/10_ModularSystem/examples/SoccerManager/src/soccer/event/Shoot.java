/* Copyright © 2017 Oracle and/or its affiliates. All rights reserved. */

package soccer.event;


public class Shoot extends GameEvent {
    
 
    public Shoot(){
        
   
        
    }
    
    public String toString() {
        return "Shoots ";
    }
    
    public GameEvent[] getNextEvents() {
        GameEvent theEvent[] = { new Goal(), new Kickout()};
        return theEvent;
    }
    
    public boolean changePlayer() {
        return false;
    }
    
    public boolean changeTeam() {
        return false;
    }
    
        /**
     * @param ballPos the ballPos to set
     */
    public void setBallPos(int currBallPos) {
       
        super.ballPos = currBallPos;

    }
    
}
