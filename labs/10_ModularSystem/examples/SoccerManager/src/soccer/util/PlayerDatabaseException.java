/* Copyright © 2017 Oracle and/or its affiliates. All rights reserved. */

package soccer.util;


public class PlayerDatabaseException extends Exception{
    
    public PlayerDatabaseException(String message){
        super(message);
    }
    
}
