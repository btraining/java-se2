# JAVA SE 2

## Unit 1 : Fast Track to Object Oriented Programming

### Java OOP Review
#### The JVM
Java classes compile down in to ByteCode, which is run by the JVM.
The JVM translates this to native instructions.<br />
The JIT (just in time) compiler runs in a separate thread in the JVM. This compiles invoked methods and passes it back in to memory in JFrames. <br />
The JIT continuously optimises by using inlining.
If multiple methods are invoked in turn like the below
```
Method1
       |__ Method2
                  |__ Method3
```
Then the JIT compiles this into a single JFrame, which is much quicker to access. <br />
Stack for local variables.
New Stack for every method call.<br />
Heap for objects.

`String string = "string";` is more efficient than using `new String` because it uses `StringBuilder` under the hood.

enums :: `ordinal` returns an int value equal to the enum constant's ordinal position, starting from 0.

NB: Once a class is found on the classpath, it will not be reloaded, even if it is found again.

##### Garbage Collection
```
          ,> Survivor Space
          |
      ---------  
 EDEN  S0...Sn    Long Tenured
|====|====|====|===============--------:
|   -->   |   -->                      :
|====|====|====|===============--------:
     |         |
     |         '-> Major Garbage collection. Anything that has existed for several
     |             runs of the minor GC can be assumed to be needed long term,
     |             and is moved in to the long tenured zone.
     |             The survivor zones can then be cleaned out.
     |
     |
     |
     '-> Minor Garbage Collection - clears out Eden, moves blocks that are
         still referenced into the survivor space. On the next run,
         anything still referenced is moved in to the next survivor space.
         Eden can then be cleaned down again and persistent things moved in to S0.
```
if Garbage Collection is running, nothing else is. The longer GC takes, then longer the application 'hangs'.

### Error Handling
Checked Exceptions: Must be handled and dealt with <br/>
Unchecked (Runtime / Error) Exceptions: An issue with the code, should _generally_ be left so that the code can be fixed.

**Throw low, catch high**

#### Assertions
`java -ea <Program>.java` to enable assertions in the JVM

### Interfaces
Using multiple inheritance, if you want to call a `super` of a specific interface, you can call `ParentName.super.method();`

### Generics and Collections
#### Generics
Generics naming convention:
```
T:       Type
E:       Element
K:       Key
V:       Value
S,U,...: Used if there are second, third, or more types
```
##### Wildcards
`? super T` - this class and any of it's super types.
`? extends T` - this class and any of it's subtypes.

#### Collections
`Comparator` - Multiple Sort options - `compare`
`Comparable` - Single sort option - `compareTo`

##### Convenience operators
`of` overloaded for 0-10 elements - this is because this accounts for most use cases, and therefore cuts down on the extra overhead of processing varargs (for more than 10).
`ofEntries` used for Maps with more than 10 entries.

## Unit 2 : Functional Programming
### Functional Interfaces
An interface with only one method.<br/>
Example: `java.util.Comparator`.

### Lambdas
Anonymous Function.<br/>
Provides parameterisation of **behaviour** as opposed to **values** or **type**.

Lambda Expression
```
robocallEligableDrivers(Person p) -> p.getAge() >= 16);
           parameter   <----'              '----> body
```
Lambdas also have type inference.<br />
Inferred and explicitly typed parameters can **not** be mixed.
#### Predicates
`Predicates` can be used as the assignment target for a lambda expression.
This means that where-ever Predicate is expected, we can pass a lambda expression.
```
public static List<Employee> filterEmployees (List<Employee> employees,
                                              Predicate<Employee> predicate)
{
  return employees.stream()
                  .filter( predicate )
                  .collect(Collectors.<Employee>toList());
}

public static Predicate<Employee> isOverEighteen() {
  return p -> p.getAge > 18;
}

// We can use a method call that returns a Predicate:
filterEmployees(employeeList, isOverEighteen());
// OR we can put a lambda directly in place of the Predicate parameter
filterEmployees(employeeList, (p) -> p.getAge > 18);
```
#### Collections Streams and Filters
`forEach` has been added to all Collections.

```
List<Object> list;

list.forEach(o -> System.out.println(o));
```

Streams are one-way (can't go backwards), and should not mutate the contents of the collection. Filters take in a `Predicate` as their argument.
```
list.stream()
    .filter(o -> o.getAge() > 21)
    .forEach(o -> System.out.println(o));
```

New collections can be created based on the output of the `filter`(s).<br/>
`filter` calls can be chained for better readbility.

Method references can be used in place of a lambda that just calls a class method.
```
// Using lambdas
forEach(t -> t.getSummary());

// Using method references
forEach(Type::getSummary);
```

#### Built in Functional Interfaces
Predicate -- used in filter.
```
public interface Predicate<T> {
  public boolean test(T t);
}
```
Consumer -- used in forEach
```
public interface Consumer<T> {
  public void accept(T t)
}
```
Function -- takes one generic type and returns another
```
public interface Function<T, R> {
  public R apply(T t);
}
```
Supplier -- returns Generic, takes no parameters. Examples include calling a builder directly from the lambda
```
public interface Supplier<T> {
  public T get();
}
```
BiPredicate -- takes two different types and performs a custom comparison
```
public interface BiPredicate<T, U> {
  public boolean test(T t, U u);
}
```
#### Lambda Operations


## Unit 3 : Modular Programming
#### The Module System
A module is a set of packages that make sense grouped together.<br/>
Introduced in Java 9.<br/>
Some module packages are
  - Exported packages: Intended for use by code outside of the Module
  - Concealed packages: Internal to the module, can be used by code inside the module only.

A module defines that it needs another module using the `requires` directive. This specifies a normal module dependency.<br/>
`requires transitive` specifies a dependency and also makes the module available to other modules.<br/>
`requires static` indicates module dependency at compile time but not at run time.

A module defines what content it makes available for other modules using the `exports` directive.<br/>
`exports ... to`  directive restricts the availablility of an exported package to a list of specifc modules. This accepts a comma separated list after the `to` keyword.

##### JLink
`jlink` builds a **platform specific** runtime environment, which means the application can be deployed on any system without the need to install the necessary JRE.

## Unit 4 : Streams and Parallel Streams
#### Concurrency
```
JTSE - Java Thread Scheduling Engine
NTSE - Native Thread Scheduling Engine
  |
  '--> Passes threads down to the cores in caches
```
Threads can run as a daemon or non-daemon.<br/>
The main thread is non-daemon.  Other threads, by default, also inherit this unless specifically marked as a daemon.

`CyclicBarrier` blocks until a specified number of threads are waiting for the thread to complete. `barrier.await();`

#### Parallel Streams
```
list.parallelStream()
// OR
list.stream()
    .filter(...)
    .filter(...)
    .peek(...)
    .parallel()
    .sum();
```

```
newList = list.parallelStream() // good parallel - stateless
              .filter(...)
              .collect(Collectors.toList());

list.parallelStream() // bad parallel - stateful - does not actually parallelise
    .filter(...)
    .collect(e -> newList.add(e));
```

Streams are deterministic for the most part. This means that given a particular input, they will always produce the same output. To sum, for example, it does not matter which order the numbers are added up in. However, some actions will yield different results if performed sequentially to if they were performed in parallel.

#### Collectors
`flat mapping` can flatten lists of lists into a single list

**TODO** Make notes on `groupingBy`

#### Custom Streams
`Spliterator` - Parallel analogue of `Iterator`. Includes decomposition for parallel processing.


## Unit 5 : Java API Programming and Secure Coding Concepts
#### Security Overview
[Oracle secure coding guidlines](https://www.oracle.com/technetwork/java/seccodeguide-139067.html)

#### JDBC
Java Database Control Layer

![JDBC component diagram](jdbc.png)

**Note** Vendors can generate their own exceptions

`Connection` - holds the DB Connection

`Statement` - an object (from the connection) to execute a static SQL statement and get the result. Queries are just `String`s.

`ResultSet` - represents a database result set.
`ResultSet` has an implementation of `next()`, so we can retrieve each result and verify that the appropriate operations have been performed.

#### Localisation

**TODO**
