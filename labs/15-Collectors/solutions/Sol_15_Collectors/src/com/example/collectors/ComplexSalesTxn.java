package com.example.collectors;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 */
public class ComplexSalesTxn {
    private long txnId;
    private String salesPerson;
    private Buyer buyer;
    private List<LineItem> lineItems;
    private LocalDate txnDate;
    private String city;
    private State state;
    private String code;
    
    public static class Builder{
    
        private long txnId = 0;
        private String salesPerson = "";
        private Buyer buyer;
        private List<LineItem> lineItems;
        private LocalDate txnDate = LocalDate.of(1, 1, 1);
        private String city = "";
        private State state;
        private String code = "";

//        private Builder() {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        }
       
        public ComplexSalesTxn.Builder txnId(long val){
            this.txnId = val;
            return this;
        }
        
        
        public ComplexSalesTxn.Builder salesPerson( String val){
            this.salesPerson = val;
            return this;
        }
        
        public ComplexSalesTxn.Builder buyer(Buyer val){
            this.buyer = val;
            return this;
        }
                
        public ComplexSalesTxn.Builder lineItems(List<LineItem> val){
            this.lineItems = val;
            return this;
        }


        
        public ComplexSalesTxn.Builder txnDate(LocalDate val){
            this.txnDate = val;
            return this;
        }    

        public ComplexSalesTxn.Builder city(String val){
          city = val;
          return this;
        }

        public ComplexSalesTxn.Builder state(State val){
          state = val;
          return this;
        }

        public ComplexSalesTxn.Builder code(String val){
          code = val;
          return this;
        }

        public ComplexSalesTxn build(){
          return new ComplexSalesTxn(this);
        }
  }
    
    private ComplexSalesTxn(){
        super();
    }
    
  private ComplexSalesTxn(ComplexSalesTxn.Builder builder){
    txnId = builder.txnId;
    salesPerson = builder.salesPerson;
    buyer = builder.buyer;
    lineItems = builder.lineItems;

    txnDate = builder.txnDate;
    city = builder.city;
    state = builder.state;
    code = builder.code;
    
  }
  
  
  public long getTxnId(){
    return txnId;
  }
  
  public String getSalesPerson(){
    return salesPerson;
  }
  
  public Buyer getBuyer(){
    return buyer;
  }
  
  public String getBuyerName(){
    return buyer.getName();
  }
    
  public List<LineItem> getLineItems(){
    return lineItems;
  }

  
  public double getTaxRate(){
      return TaxRate.byState(this.getState());
  }
  
  public double getDiscountRate(){
      return this.getBuyer().getBuyerClass().getRate();
  }
  
  public LocalDate getTxnDate(){
      return txnDate;
  }
    
  public String getCity(){
      return city;
  }
  
  public State getState(){
      return state;
  }
  
  public String getCode(){
      return code;
  }
 
//  public double getTransactionTotal(){
//      return this.unitCount * this.unitPrice;
//  }
 
  public static int sortByBuyer(ComplexSalesTxn a, ComplexSalesTxn b){
      return a.getBuyerName().compareTo(b.getBuyerName());
  }
  
  public String print(){
      return
        "Transaction id: " + txnId + "\n" +
        "Sales person: " + salesPerson + "\n" +
        "Buyer name: " + this.getBuyerName() + "\n" +
        "Buyer class: " + this.getBuyer().getBuyerClass() + "\n" +
        "Line items: " + lineItems + "\n" + 

        "Tax rate: " + this.getTaxRate() + "\n" +
        "Discount rate: " + this.getDiscountRate() + "\n" +
        "Transaction date: " + txnDate + "\n" +
        "City: " + city + "\n" +
        "State: " + state + "\n" +
        "Code: " + code + "\n";
  } 

    public void printSummary(){
        System.out.println("ID: " + txnId + " - " +
        "Seller: " + salesPerson + " - " +
        "Buyer: " + this.getBuyerName() + " - " +
        "Line items: " + lineItems + " - " + 
        "ST: " + state + " - " + 

        "Date: " + txnDate);      
    }

    public String getSummaryStr(){
        return
        "ID: " + txnId + " - " +
        "Seller: " + salesPerson + " - " +
        "Buyer: " + this.getBuyerName() + " - " +
        "Line items: " + lineItems + " - " + 
        "ST: " + state + " - " + 

        "Date: " + txnDate;      
    }


  @Override
  public String toString(){
    return "Transaction id: " + txnId +
        "Sales person: " + salesPerson +
        "Buyer name: " + this.getBuyerName() + 
        "Buyer class: " + this.getBuyer().getBuyerClass() +
        "Line items: " + lineItems + 

        "Tax rate: " + this.getTaxRate() +
        "Discount rate: " + this.getDiscountRate() +
        "Transaction date: " + txnDate +
        "City: " + city + 
        "State: " + state +
        "Code: " + code + "\n";
  } 

  public static List<ComplexSalesTxn> createTxnList(){
    List<ComplexSalesTxn> txnList = new ArrayList<>();
    Map<String, Buyer> buyerMap = Buyer.getBuyerMap();
    
    txnList.add(new ComplexSalesTxn.Builder()
            .txnId(11)
            .salesPerson("Jane Doe")
            .buyer(buyerMap.get("Acme"))
            .lineItems(List.of(new LineItem("Widget",20, 300),new LineItem("Widget Pro II",30, 1500)))
            .txnDate(LocalDate.of(2013,1,25))
            .city("San Jose")
            .state(State.CA)
            .code("95101")
            .build() 
    );
    
    
    txnList.add(new ComplexSalesTxn.Builder()
            .txnId(12)
            .salesPerson("Jane Doe")
            .buyer(buyerMap.get("Acme"))
            .lineItems(List.of(new LineItem("Widget Pro",40, 500)))
            .txnDate(LocalDate.of(2013,4,5))
            .city("San Jose")
            .state(State.CA)
            .code("95101")
            .build() 
    );
    
    
    txnList.add(new ComplexSalesTxn.Builder()
            .txnId(13)
            .salesPerson("Rob Doe")
            .buyer(buyerMap.get("RadioHut"))
            .lineItems(List.of(new LineItem("Widget Pro II",35, 1500), new LineItem("Widget",20, 300)))
            .txnDate(LocalDate.of(2013,10,3))
            .city("San Jose")
            .state(State.CA)
            .code("95101")
            .build() 
    );
    
    txnList.add(new ComplexSalesTxn.Builder()
            .txnId(14)
            .salesPerson("John Smith")
            .buyer(buyerMap.get("GreatDeals"))
            .lineItems(List.of(new LineItem("Widget",120, 300), new LineItem("Widget Pro",40, 500), new LineItem("Widget Pro II",40, 1500)))
            .txnDate(LocalDate.of(2013,10,10))
            .city("San Jose")
            .state(State.CA)
            .code("95101")
            .build() 
    );
    txnList.add(new ComplexSalesTxn.Builder()
            .txnId(15)
            .salesPerson("Betty Jones")
            .buyer(buyerMap.get("RadioHut"))
            .lineItems(List.of(new LineItem("Widget Pro",41, 500), new LineItem("Widget",34, 300)))
            .txnDate(LocalDate.of(2013,2,4))
            .city("Denver")
            .state(State.CO)
            .code("80216")
            .build() 
    );
    
    txnList.add(new ComplexSalesTxn.Builder()
            .txnId(16)
            .salesPerson("Betty Jones")
            .buyer(buyerMap.get("BestDeals"))
            .lineItems(List.of(new LineItem("Widget Pro II",14, 1500)))
            .txnDate(LocalDate.of(2013,3,21))
            .city("Denver")
            .state(State.CO)
            .code("80216")
            .build() 
    );
    
    txnList.add(new ComplexSalesTxn.Builder()
            .txnId(17)
            .salesPerson("Dave Smith")
            .buyer(buyerMap.get("PriceCo"))
            .lineItems(List.of(new LineItem("Widget",55, 300)))
            .txnDate(LocalDate.of(2013,3,20))
            .city("Denver")
            .state(State.CO)
            .code("80216")
            .build() 
    );
    
    
    txnList.add(new ComplexSalesTxn.Builder()
            .txnId(18)
            .salesPerson("Dave Smith")
            .buyer(buyerMap.get("PriceCo"))
            .lineItems(List.of(new LineItem("Widget Pro II",12, 1500)))
            .txnDate(LocalDate.of(2013,3,30))
            .city("Denver")
            .state(State.CO)
            .code("80216")
            .build() 
    );
    txnList.add(new ComplexSalesTxn.Builder()
            .txnId(19)
            .salesPerson("Betty Jones")
            .buyer(buyerMap.get("BestDeals"))
            .lineItems(List.of(new LineItem("Widget Pro II",80, 1500)))
            .txnDate(LocalDate.of(2013,7,12))
            .city("Denver")
            .state(State.CO)
            .code("80216")
            .build() 
    );
    
    txnList.add(new ComplexSalesTxn.Builder()
            .txnId(20)
            .salesPerson("John Adams")
            .buyer(buyerMap.get("PriceCo"))
            .lineItems(List.of(new LineItem("Widget Pro II",34, 1500)))
            .txnDate(LocalDate.of(2013,7,14))
            .city("Boston")
            .state(State.MA)
            .code("02108")
            .build() 
    );
    
    txnList.add(new ComplexSalesTxn.Builder()
            .txnId(21)
            .salesPerson("John Adams")
            .buyer(buyerMap.get("PriceCo"))
            .lineItems(List.of(new LineItem("Widget",20, 300)))
            .txnDate(LocalDate.of(2013,10,6))
            .city("Boston")
            .state(State.MA)
            .code("02108")
            .build() 
    );
    
    txnList.add(new ComplexSalesTxn.Builder()
            .txnId(22)
            .salesPerson("Samuel Adams")
            .buyer(buyerMap.get("MomAndPops"))
            .lineItems(List.of(new LineItem("Widget Pro II",45, 1500)))
            .txnDate(LocalDate.of(2013,10,2))
            .city("Boston")
            .state(State.MA)
            .code("02108")
            .build() 
    );
    
    txnList.add(new ComplexSalesTxn.Builder()
            .txnId(23)
            .salesPerson("Samuel Adams")
            .buyer(buyerMap.get("RadioHut"))
            .lineItems(List.of(new LineItem("Widget Pro",18, 500), new LineItem("Widget",37, 300)))
            .txnDate(LocalDate.of(2013,12,8))
            .city("Boston")
            .state(State.MA)
            .code("02108")
            .build() 
    );
    
    
    return txnList;
  }
  
}
