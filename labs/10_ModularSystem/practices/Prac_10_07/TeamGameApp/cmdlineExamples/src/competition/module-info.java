/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//
// Note no requirement to soccer or basketball
//

module competition {
    requires transitive factory; // As main calling app needs access to the Factory
    // really like requires AND exports
    requires transitive gameapi; // 
    requires transitive ascii_display;
    //requires jackson.annotations;
    requires jackson.core;  // Only jackson.core required?
    //requires jackson.databind;
    exports tournamentType;
}
