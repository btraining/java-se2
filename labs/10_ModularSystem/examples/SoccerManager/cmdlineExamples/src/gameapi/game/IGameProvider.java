/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.time.LocalDateTime;

/**
 *
 * 
 */
public interface IGameProvider {
    
    IGame getGame(ITeam homeTeam, ITeam awayTeam, LocalDateTime plusDays);
    
    IPlayer getPlayer(String playerName);
    
    ITeam getTeam(String teamName, IPlayer[] players);
    
    String getType();
    
}
