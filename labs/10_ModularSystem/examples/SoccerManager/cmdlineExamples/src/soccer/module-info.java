/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

module soccer {
   
    requires gameapi;
    opens soccer.play;  // As alternative to opening entire module
    opens soccer.event;

    // Note - no need to export anything as provider supplies
    // the necessary
    
    // Using concrete classes example
    //provides game.IGame with soccer.play.Soccer;
    //provides game.IPlayer with soccer.play.Player;
    //provides game.ITeam with soccer.play.Team;
    
    // Using a provider classes instead
    provides game.IGameProvider with soccer.play.GameProvider;

}
