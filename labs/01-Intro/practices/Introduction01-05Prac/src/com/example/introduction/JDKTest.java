/* Copyright © 2017 Oracle and/or its affiliates. All rights reserved. */

package com.example.introduction;

public class JDKTest {
    public static void main(String[] args) {

        System.out.println("Java Version: " +System.getProperty("java.version"));
        
    }
}
