/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package customAnnotation;

import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(TYPE)
public @interface PreamblesContainer {
    ClassPreamble[] value();
}

