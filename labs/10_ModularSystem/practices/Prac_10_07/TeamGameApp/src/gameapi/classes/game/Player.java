/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;


import java.io.Serializable;

/**
 *
 * 
 */
public interface Player extends Comparable, Serializable {

    String getDetailType();

    // TODO - possibly remove this so can sort based on Lambda expression
    // If so, need to use 1.8 Java for GlassFish
    int compareTo(Object thePlayer);
    // TODO (8/2016)
    // Add code here to have player return suggested Event
    //
    // Add playerPosition int
    // Add various playerSkill values
    //
    // Add method for Players to move when off the ball
    //

    /**
     * @return the goalsScored
     */
    int getGoalsScored();

    /**
     * @return the playerName
     */
    String getPlayerName();

    void incGoalsScored();

    /**
     * @param goalsScored the goalsScored to set
     */
    void setGoalsScored(int goalsScored);

    /**
     * @param playerName the playerName to set
     */
    void setPlayerName(String playerName);
    
}
