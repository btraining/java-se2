/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 *
 * 
 */
// This abstract class could be used to move the custom Serialization and Deserialization 
// out of the IGame implementations. Then it's not necessary to for these implementations 
// to have a dependency on jackson.
// But the solution used in JacksonUtil is cleaner.
//
// Another reason is to move some of the common methods here, but have the implementers
// of IGame (and thus this abstract class) implement the unique methods like playGame().
// Note that the main difference between Basketball and Soccer is in playGame() but 
// also in the GameEvents and how they're interelated.

public abstract class GameType {
    
}
