/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package pkgB;

import java.lang.reflect.Field;
import pkgA.ClassA;

public class NewMain {


    public static void main(String[] args) {
        ClassA test = new ClassA();
        System.out.println(test.YEAR);
        
        //Try accessing private data through reflection
        /*
        try {
            Field secret = Class.forName("pkgA.ClassA").getDeclaredField("secretCrush");
            secret.setAccessible(true);
            System.out.println(secret.get(test));
            
        } 
        catch (Exception ex) {
            ex.printStackTrace();
        }
        */
    }   
}
