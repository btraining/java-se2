/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package pkgA;

public class ClassA {
    public static final int YEAR = 1974;
    private String secretCrush = "Kenny O'Kelly";
}
