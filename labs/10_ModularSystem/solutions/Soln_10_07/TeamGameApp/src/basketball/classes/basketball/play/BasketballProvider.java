/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basketball.play;

import java.time.LocalDateTime;
import game.Game;
import game.GameProvider;
import game.Player;
import game.Team;

/**
 *
 * 
 */
public class BasketballProvider implements GameProvider {
    
    public Game getGame(Team homeTeam, Team awayTeam, LocalDateTime plusDays) {
        return new Basketball(homeTeam, awayTeam, plusDays);
    }

    public Player getPlayer(String playerName){
        return new BasketballPlayer(playerName);
    }
    
    public Team getTeam(String teamName, Player[] players){
        return new BasketballTeam(teamName, players);
    }
    public String getType() {
        return "Basketball";
    }
}
