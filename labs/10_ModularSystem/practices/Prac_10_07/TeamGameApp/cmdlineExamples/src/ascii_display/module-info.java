/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

module ascii_display {
    //requires jackson.annotations;
    requires jackson.core;  // Only jackson.core required as it in turn requires all the other jacksons 
    opens displaytree;                       // transitively (one must presume)
    //requires jackson.databind;
    exports displaytree;
}
