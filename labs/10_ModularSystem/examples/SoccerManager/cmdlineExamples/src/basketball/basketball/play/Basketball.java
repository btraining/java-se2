/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package basketball.play;

import game.IGame;
import java.io.Serializable;
//import java.time.LocalDate;
import basketball.event.Kickoff;
import basketball.event.GameEvent;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import game.IGameEvent;
import game.IGameResult;
import game.IPlayer;
import game.ITeam;
import basketball.util.Settings;


/**
 *
 * 
 */
public class Basketball implements IGame {
    
	// Should GameResult be a member of Basketball? (Rather than be 
	// calculated each time required.

    private ITeam homeTeam;
    private ITeam awayTeam;
    private IGameEvent[] events;
    
    // This stops the code from trying to do reflection but 
    // the serializer and deserializer don't get called and so 
    // saves null to JSON file

    private LocalDateTime theDateTime;
    
    private boolean detailAvailable = false;
    private int id = 0;
    private String detailType = "Basketball";

    //public boolean isHomeTeam; // = true; // A game starts with home team
    
//    public Basketball(Team homeTeam, Team awayTeam, LocalDateTime theDateTime) {
//        this.homeTeam = homeTeam;
//        this.awayTeam = awayTeam;
//        this.theDateTime = theDateTime;
//    }
    public Basketball() {};

    public Basketball(ITeam homeTeam, ITeam awayTeam, LocalDateTime plusDays) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.theDateTime = theDateTime;
    }
    
    public void populate (ITeam homeTeam, ITeam awayTeam, LocalDateTime plusDays) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.theDateTime = theDateTime;
    }


    public void playGame() {

	// Set up an Event list with Kickoff as the first Event (Need an ArrayList for convenience
	// but this will be copied to an events Array at the end of the loop that creates the events
	// Choose either homeTeam or awayTeam to have possession
	// Choose a player to kickoff randomly from the team that has possession

        ArrayList<IGameEvent> eventList = new ArrayList();
        ITeam currTeam;
        IPlayer currPlayer;
        IGameEvent currEvent = new Kickoff();
	IGameEvent nextEvent;
        currEvent.setBallPos(50); // Maybe this should happen automatically as part of Kickoff?
        currEvent.setTheTeam(Math.random() > 0.5?homeTeam: awayTeam);
        currEvent.setThePlayer(currEvent.getTheTeam().
                getPlayerArray()[(int) (Math.random() * this.homeTeam.getPlayerArray().length)]);
        currEvent.setTheTime(0);
        eventList.add(currEvent);

	// Loop until game is over (longer than 90mins)
	// if statement ensures that the increase of the time (i) increases by random jumps for each event.

        for (int i = 1; i <=Settings.GAME_LENGTH; i++){
            
            //if (Math.random() > Settings.GAME_EVENT_FREQUENCY){
            if (true) {
                
                // First need to get currTeam, currPlayer, and currBallPos in case we need to change.
                // TODO - move Type declaration outside for loop
                currTeam = currEvent.getTheTeam();
                currPlayer = currEvent.getThePlayer();
                int currBallPos = currEvent.getBallPos(); // Need to get before setting up new Event
                
                // Instead ask Team in possession what should be next event
                // Instead of being team-based, should this be player-based (8/2016)? Prob not, but based on
		// player skills

		// Currently this is random, based on what is a poss
		// Each Event defines what is a valid next event and if it means a 
		// team change, player change, or both

		// Current team in possession returns the event they'd like their player to try doing
		// Circumstances:
		// - player skill
		// - ball position relative to goalmouth
		// - etc.
		// will be used by the playing engine to determine if this action is successfull or the attempt fails 
		// in some way
		//

		// Temporary simple random determine failure conditional
		if ((Math.random()* 100 < currEvent.getPercentChanceSuccess())){
		    ///System.err.println("Succeeded " + currEvent);
		    // ** This will replace currEvent with next event **//
		    // Need to check validity
		    IGameEvent tempEvent = currTeam.getNextPlayAttempt(currEvent); 

		    try {
			IGameEvent[] tempEvents = currEvent.getNextEvents();
			boolean isFound = false;

			    for (IGameEvent thisEvent: tempEvents ) {
				    if ( thisEvent.getClass().equals( tempEvent.getClass())) { 
				    // We're good
				    isFound = true;
				    }
			    }

			    if (isFound) {
				currEvent = tempEvent;
			    } else throw new Exception("Not a valid event type.");
			} catch (Exception e) {
			    System.out.println(tempEvent + " not a valid event type for "+ currEvent);
			}
		} else {
		    //System.err.println("Failed" + currEvent);
		    currEvent = currEvent.getNextFailEvent()[0];
		}		
		    //currEvent = currTeam.getNextPlayAttempt(currEvent); 

                // This is now the new currEvent so need to know if should change player and team
                //currEvent.setTheTeam(currEvent.changeTeam()?getOtherTeam(currTeam): currTeam);
		// Instead of all this juggling with currThis and currThat, should we just have currEvent and nextEvent? (8/2016)
                
                // Now set the new ball position for this nextEvent.
                currEvent.setBallPos(currBallPos );  // 8 could be random.
                
                if (currEvent.changeTeam()) {
                    currTeam = getOtherTeam(currTeam);
                    // Reverse ball position
                    currEvent.reverseBallPos();
                    //currEvent.changeTeamPossession();
                }
                currEvent.setTheTeam(currTeam);
                
                //System.out.println(currEvent.getTheTeam().getTeamName());
               
		// Below code sets things up so not same player twice 
		// It actually doesn't need to run when there's a change in team (in that case currPlayer won't be removed
                ArrayList <IPlayer> currPlayerList = new ArrayList(Arrays.asList(currEvent.getTheTeam().getPlayerArray()));
                if (currPlayerList.size() > 1 ) currPlayerList.remove(currPlayer); // Needs a check here otherwise error
                currEvent.setThePlayer(
                    currEvent.changePlayer()?
                    currPlayerList.get((int)(Math.random() * currPlayerList.size())):
                    currPlayer
                );

                currEvent.setTheTime(i);
		// Note that currTeam, currPlayer, currBallPos are all now for the previous event
		// but they will be updated at the start of the loop
                eventList.add(currEvent); 
                //System.out.println(i);
            }

	    // Finally copy the list of created Events
            this.events = new IGameEvent[eventList.size()];
            eventList.toArray(events);
 
        }
    }
    

    public String getDescription(boolean showEvents) {

        // Announce the game
        StringBuilder returnString = new StringBuilder();
        returnString.append(this.getHomeTeam().getTeamName() + " vs. " +
                this.getAwayTeam().getTeamName() + " (" + 
                this.getTheDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE) + ")");
        returnString.append("\n");
        
        // Returns result object based on the gameEvents array
        //GameResult theResult = new GameResult(this);
        IGameResult theResult = getGameResult();
      
        // Announce result
        if (theResult.isDrawnGame()){
            returnString.append("It's a draw!");
        } else  {
            returnString.append(theResult.getWinner().getTeamName());
            returnString.append(" win!");
        }
        returnString.append(" (" + theResult.getHomeTeamScore() + " - " + theResult.getAwayTeamScore() + ") \n");
	
        
        // Add description of the events  
        if (showEvents){
            returnString.append("Ball position distance is relative to team currently in possession for the Event.\n\n");
            for (IGameEvent currEvent: this.getEvents()) {
                returnString.append(currEvent.getBallPos() + " : " + currEvent + "after " + 
                currEvent.getTheTime() + " mins by " + 
                currEvent.getThePlayer().getPlayerName() + 
                " of " + currEvent.getTheTeam().getTeamName() + 
                "\n");
            }  
        }

        return returnString.toString();
    }
    
    

    public String getDescription() {
        return getDescription(false);
    }
    

    public String getScore(){
        
        String theScore;
        IGameResult theResult = getGameResult();
        theScore = theResult.getHomeTeamScore() + " - " + theResult.getAwayTeamScore();
        return theScore;
        
    }
    
    // TODO - is there neater way? Ternary works.
  
    public ITeam getOtherTeam(ITeam currTeam){
        if (currTeam == homeTeam){
            currTeam = awayTeam;
        } else currTeam = homeTeam;
        return currTeam;
    }

    /**
     * @return the homeTeam
     */

    public ITeam getHomeTeam() {
        return homeTeam;
    }

    /**
     * @param homeTeam the homeTeam to set
     */
   
    public void setHomeTeam(ITeam homeTeam) {
        this.homeTeam = homeTeam;
    }

    /**
     * @return the awayTeam
     */
 
    public ITeam getAwayTeam() {
        return awayTeam;
    }

    /**
     * @param awayTeam the awayTeam to set
     */

    public void setAwayTeam(ITeam awayTeam) {
        this.awayTeam = awayTeam;
    }

    /**
     * @return the events
     */

    public IGameEvent[] getEvents() {
        return events;
    }

    /**
     * @param events the events to set
     */

    public void setEvents(IGameEvent[] events) {
        this.events = events;
    }

    /**
     * @return the localDateTime
     */
 
    public LocalDateTime getLocalDateTime() {
        return getTheDateTime();
    }

    /**
     * @param theDateTime the localDateTime to set
     */

    public void setLocalDateTime(LocalDateTime theDateTime) {
        this.setTheDateTime(theDateTime);
    }

    
    /**
     * @return the theDateTime
     */

    public LocalDateTime getTheDateTime() {
        return theDateTime;
    }

    /**
     * @param theDateTime the theDateTime to set
     */

    public void setTheDateTime(LocalDateTime theDateTime) {
        this.theDateTime = theDateTime;
    }
    
    
    
    // TODO. Perhaps better have the code here, not in constructor of GameResult

    public IGameResult getGameResult(){
        return new GameResult(this);  // ???
    }
    
        // Remainder is displayDetailStuff
    

    public String getDisplayDetail(){
        return getScore();
    }
    @Override
    public boolean isDetailAvailable (){
        return detailAvailable;
    }
    @Override
    public int getID(){
        return id;
    }
    @Override
    public String getDetailType() {
        return detailType;
    }

    /**
     * @param detailAvailable the detailAvailable to set
     */

    public void setDetailAvailable(boolean detailAvailable) {
        this.detailAvailable = detailAvailable;
    }

    /**
     * @return the id
     */
    //public int getId() {
    //    return id;
    //}

    /**
     * @param id the id to set
     */

    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the getDetailType
     */

    public String getGetDetailType() {
        return detailType;
    }

    /**
     * @param getDetailType the getDetailType to set
     */

    public void setGetDetailType(String detailType) {
        this.detailType = detailType;
    }
    
        @Override
    public String getScoreDescriptionString() {
        return "Baskets";
    }
    
      
}
