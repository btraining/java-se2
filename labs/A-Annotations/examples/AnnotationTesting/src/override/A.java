/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package override;

public class A {
    
    void method(int x){
        System.out.println("A");
    }
}
