/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package safeVarargs;

import java.util.List;


public class TestVarargs {
    
    //Safe example
    //@SafeVarargs
    static void m(List<Integer> ... args){
        System.out.println(args.length);
    }  
/*    
    //Unsafe example
    @SafeVarargs
    static void m2(List<Integer> ... args){    
        Object[] objectArray = args;
        objectArray[0] = List.of("Bug");
        int broken = args[0].get(0);
    }  
*/
    public static void main(String[] args) {
        m(List.of(100, 200, 300));
        //m2(List.of(400, 500, 600));
    }
}
