/* Copyright © 2017 Oracle and/or its affiliates. All rights reserved. */

package soccer.play;


public interface IDisplayDataItem {
    
    public boolean isDetailAvailable ();
    public String getDisplayDetail();
    public int getID();
    public String getDetailType();    // return simple text String or return the actual object?
    
}
