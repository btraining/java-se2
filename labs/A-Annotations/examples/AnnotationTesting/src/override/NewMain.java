/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package override;

public class NewMain {

    public static void main(String[] args) {
        B exampleInstance = new B();
        exampleInstance.method(0); //expected B to print
    }
    
}
