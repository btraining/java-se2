 package tournament;

import displayDiagram.Display;
import displayDiagram.DisplayDetail;
//import displayDiagram.DisplayString;
import game.Game;
import java.util.StringTokenizer;
import tournamentType.Knockout;
import gamefactory.GameFactory;
import tournament.util.PlayerDatabase;
import tournament.util.PlayerDatabaseException;
import tournamentType.League;
import game.Team;

public class Main {
    
    static String dirName = "data";
    
//To create and display a tournament:
//
// - Create teams (randomly from a database of players) or by loading from a file
// - Create a competition based on a style(currently OldLeague or Knockout are available)
//   and pass optionally pass in the list of teams (they can be added later OR are
//   not required if loading a set of completed games from a file.

// - Play all games OR load a set of completed games for that sport 
//      and competition style
// - Call various methods on the competition to display the results of the completed games
    
// TODO - Either create the competition using teams, OR using filename to load completed competition from
    
    
// Current setup is using an GameProvider to deliver all the concrete implementations of IGame, Team, and Player
// getProvider is being called each time needed, but maybe that's okay? TODO, check to see if same object each time
// (see Modular Java book for this).
    
    
    public static void main(String[] args) {
        
        // Create the teams
        //String teamNames = "Robins, Pelicans";
        //String teamNames = "Robins, Pelicans, Sparrows, Magpies";
        String teamNames = "Robins, Pelicans, Sparrows, Magpies, Crows, Falcons, Geese, Terns";
        //String teamNames = "Plovers,Ravens,Doves,Robins,Sparrows,Magpies,Crows,Falcons,Pelicans,Geese,Ducks,Eagles,Gulls,Hawks,Owls,Pigeons";
        int teamSize = 5;   // Number of players on each team
        Team[] theTeams = null;
        try {
            // TODO in this example team is same regardless of gameType but could be otherwise
            theTeams = createTeams("Soccer", teamNames, teamSize);  
        } catch (PlayerDatabaseException e) {
            e.printStackTrace(System.err);
        }
        
        
        //***** Knockout competition *****//
        // TODO pass in the type of competition
        Knockout theKnockoutCompetition = new Knockout(theTeams, "Soccer");   // Creates a Knockout competition
  
        // Create and play all the games
        // TODO Needs to return something to indicate success or failure (e.g. are there any teams?)
        theKnockoutCompetition.createAndPlayAllGames();
        
        //theKnockoutCompetition.saveGamesToJSONFile(dirName, "completedKnockout01.json");

        // OR, just load some completed knockout games from file
        // TODO Needs to return something to indicate success or failure 
        // (e.g. are there any games? are there correct number for knockout?)
        //Knockout 
        //theKnockoutCompetition = null;
        //theKnockoutCompetition = new Knockout(dirName, "completedKnockout01.json");
        
        
        // Display all the games
        // First get a JSON string of the details of all the games as this is 
        // what the ascii_display module requires
        //String displayResults = theKnockoutCompetition.getJSONListOfGames();
        
        // Then use ascii_display module to display results
        //Display.printLRTourney(theKnockoutCompetition.getGames());
        //Display.printLRTree(displayResults);
        
        // or
        
        //buildTreeNode(theKnockoutCompetition.getGames());
        
        //getGames() returns a one dimensional array of games
        //Display.printLRTree(theKnockoutCompetition.getGames());
        
        DisplayDetail[] dataKnockout = getData(theKnockoutCompetition.getGames());
        Display.printDataTree(dataKnockout);
        
        System.out.println();
        
        
        //***** League competition *****//
        
        // Create a new league competition by passing in teams
        League theLeagueCompetition = new League(theTeams, "Soccer");
        
        // Create and play all games using these teams
        theLeagueCompetition.createAndPlayAllGames();
        
        // Save the games in JSON format to a file
        //theLeagueCompetition.saveGamesToJSONFile(dirName, "completedLeague03.json");
        
        // Or get the completed games from a file.
        //League 
        //theLeagueCompetition = null;
        //theLeagueCompetition = new League(dirName, "completedLeague03.json");

        
        // Get the display grid in JSON format
        //DisplayString[][] dataGrid = getLeagueDataGrid(theLeagueCompetition.getGames(), theLeagueCompetition.getTeams()); 
       
        // Call Display type required
        //Display.outputTextLeagueGrid(dataGrid); 
        
        DisplayDetail[][] dataGrid = getDataGrid(theLeagueCompetition.getGames(), theLeagueCompetition.getTeams());
        //Display.outputTextGrid(dataGrid); 
       
        
        
    }
    
            
    
    
    public static Team[] createTeams(String gameType, String teamNames, int teamSize) throws tournament.util.PlayerDatabaseException {

        PlayerDatabase playerDB = new PlayerDatabase(gameType);

        StringTokenizer teamNameTokens = new StringTokenizer(teamNames, ",");
        Team[] theTeams = new Team[teamNameTokens.countTokens()];
        for (int i = 0; i < theTeams.length; i++) {
            theTeams[i] = GameFactory.getProvider(gameType).
                    getTeam(teamNameTokens.nextToken().
                            trim(), playerDB.getTeam(teamSize));
        }

        return theTeams;
    }

    private static void buildTreeNode(Game[] games) {
        System.out.println("here");
        // iterate backwards as winner of tournament is last game of array
        for (int i = games.length; i > 0; i--){
            //System.out.println("inside here");
            System.out.println(games[i - 1].getScore());
            //TreeNode rootNode = new TreeNode()
        }
        int currNode = games.length-1;
        
        
        while (currNode > -1) {
            
            
        }
        
    }
    
        
        
    private static DisplayDetail[][] getDataGrid(Game[] theGames, Team[] theTeams) {

        int numTeams = theTeams.length;

        // Size of grid allow for extra column on the left for list of Teams, and two 
        // extra columns on right for Points and Goals. Also extra column on top for list of
        // Teams.
        DisplayDetail[][] theGrid = new DisplayDetail[numTeams + 1][numTeams + 3];

        int colNum = 0;
        int rowNum = 0;

        // Starting at 0, 0, insert a blank top left corner.
        theGrid[rowNum][colNum] = new DisplayDetail("dfdf");

        // Do the first row of Teams (headings);
        for (int i = 0; i < theTeams.length; i++) {

            theTeams[i].setId(i);   // set the Id to the index
            // ternary expression below determines whether to add the full orginal Team class or a simple 
            // DisplayString class
            theGrid[rowNum][colNum + 1] = new DisplayDetail(theTeams[i].getTeamName());
            //theGrid[rowNum][colNum + 1] = theTeams[i];
            colNum++;
        }

        // Add Points and Games columns to the first row (headings)
        theGrid[rowNum][colNum + 1] = new DisplayDetail("Points");
        // Getting "Goals" vs "Baskets" from Game (same on all IGames - another approach?)
        theGrid[rowNum][colNum + 2] = new DisplayDetail(theGames[0].getScoreDescriptionString());

        // Add each row of Games for each home team (note all Team IDs will be set by previous for loop
        // Also note rowNum = i + 1; therefore starting on second row.
        for (int i = 0; i < theTeams.length; i++) {
            rowNum = i + 1;
            
            // Add the home Team to the first column of the current row
            colNum = 0;
            Team currHomeTeam = theTeams[i];
            //theGrid[rowNum][colNum] = currHomeTeam;
            
            theGrid[rowNum][colNum] = new DisplayDetail(currHomeTeam.getTeamName());
            //theGrid[rowNum][colNum] = theTeams[i];


            // Inner loop through all away teams on current row to add Games
            for (Team currAwayTeam : theTeams) {
                colNum++;   // Could also use traditional for loop here
                if (currHomeTeam != currAwayTeam) {
                    for (Game theGame : theGames) {
                        if (theGame.getHomeTeam().getTeamName().equals(currHomeTeam.getTeamName()) 
                                && theGame.getAwayTeam().getTeamName().equals(currAwayTeam.getTeamName())) {
                            //theGrid[rowNum][colNum] = theGame;
                            //theGrid[rowNum][colNum] = useOriginalClass?theGame:new DisplayString(theGame.getScore());
                            //theGrid[rowNum][colNum] = theGame;
                            //System.out.println("Setting the score!");
                            theGrid[rowNum][colNum] = new DisplayDetail(theGame.getScore());
                            break;
                        }
                    }
                } else {
                    theGrid[rowNum][colNum] = new DisplayDetail(" X ");    // Mark with X as team doesn't play itself
                }
            }

            // Add last two columns to current row (team points and goals)
            theGrid[rowNum][colNum + 1] = new DisplayDetail(Integer.toString(currHomeTeam.getPointsTotal()));
            theGrid[rowNum][colNum + 2] = new DisplayDetail(Integer.toString(currHomeTeam.getGoalsTotal()));
        }
        return theGrid;
    }
    
        // Convert array of Game into array of DisplayDetail so display-ascii library can process
    public static DisplayDetail[] getData ( Game[] gameList) {
        
        DisplayDetail[] theDetails = new DisplayDetail[gameList.length];
        
        for (int i = 0; i < gameList.length; i++) {
            theDetails[i] = new DisplayDetail(gameList[i].getHomeTeam().getTeamName() + " " + gameList[i].getAwayTeam().getTeamName());
            theDetails[i].addDetail(gameList[i].getScore());
        }
        
        return theDetails;

    }

    
}
