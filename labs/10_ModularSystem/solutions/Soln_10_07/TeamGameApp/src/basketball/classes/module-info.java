/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Note entire module opened. See soccer for better approach of only
// necessary package opened and specifically to a module
module basketball {
    
    requires gameapi;

    // Not required as Provider provides the necessary classes - interesting!
    //exports basketball.event;
    //exports basketball.play;
    //exports basketball.util;
    
    // Using concrete classes example
    //provides game.IGame with basketball.play.Basketball;
    //provides game.IPlayer with basketball.play.Player;
    //provides game.ITeam with basketball.play.Team;
    
    // Using a provider classe instead
    
    provides game.GameProvider with basketball.play.BasketballProvider;
}
