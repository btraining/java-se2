/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamefactory;


import java.util.ServiceLoader;
import game.GameProvider;


/**
 *
 * 
 */
public class GameFactory {
    
    //   Simple version of GameFactory that doesn't use providers could just get Team
    //   and Player via service. Disadvantages are that Game, Player, and Team must 
    //   be populated then. There's also code duplication in the methods of this class.
    
    //   Another (better) approach is to have the ServiceLoader return a provider
    //   and that provider can then instantiate Game, Player, and Team as needed.

    
//    public static ITeam getTeam(String teamName, IPlayer[] players, String type) {
//        ITeam theTeam = null;
//        ITeam tempTeam = null;
//        
//        ServiceLoader<ITeam> sl
//            = ServiceLoader.load(ITeam.class);
//        Iterator<ITeam> iter = sl.iterator();
//
//        ArrayList<ITeam> providers = new ArrayList<>();
//        while (iter.hasNext()) {
//
//            providers.add(iter.next());
//            
//            // Check for the type of object we want
//            if ((tempTeam = providers.get(providers.size()-1)).getDetailType().equals(type)){
//                theTeam = tempTeam;
//            }
//
//        }
//
//        if (providers.isEmpty()) {
//            throw new RuntimeException("No service providers found!");
//        }
//        if (theTeam == null) {
//            throw new RuntimeException("No suitable service provider found!");
//        }
//        
//        //theCountry = (providers.get(1)); // Careful! Hard-coded! (index 0 so get first one)                       
//        theTeam.setTeamName( teamName);
//        theTeam.setPlayerArray(players);
//        return theTeam;
//    }    
//    public static IPlayer getPlayer(String playerName, String type) {
//        IPlayer thePlayer = null;
//        IPlayer tempPlayer = null;
//        
//        ServiceLoader<IPlayer> sl
//            = ServiceLoader.load(IPlayer.class);
//        Iterator<IPlayer> iter = sl.iterator();
//
//        ArrayList<IPlayer> providers = new ArrayList<>();
//        while (iter.hasNext()) {
//
//            providers.add(iter.next());
//            
//            // Check for the type of object we want
//            if ((tempPlayer = providers.get(providers.size()-1)).getDetailType().equals(type)){
//                thePlayer = tempPlayer;
//            }
//
//        }
//
//        if (providers.isEmpty()) {
//            throw new RuntimeException("No service providers found!");
//        }
//        if (thePlayer == null) {
//            throw new RuntimeException("No suitable service provider found!");
//        }
//        
//        //theCountry = (providers.get(1)); // Careful! Hard-coded! (index 0 so get first one)                       
//        thePlayer.setPlayerName( playerName);
//
//        return thePlayer;
//    }
//    
//    public static IGame getGame(String type, ITeam homeTeam, ITeam awayTeam, LocalDateTime dateTime) {
//
//        IGame theGame = null;
//        IGame tempGame = null;
//        
//        ServiceLoader<IGame> sl
//            = ServiceLoader.load(IGame.class);
//        Iterator<IGame> iter = sl.iterator();
//
//        ArrayList<IGame> providers = new ArrayList<>();
//        while (iter.hasNext()) {
//
//            providers.add(iter.next());
//            
//            // Check for the type of game we want
//            if ((tempGame = providers.get(providers.size()-1)).getDetailType().equals(type)){
//                theGame = tempGame;
//            }
//
//        }
////        int numProviders = providers.size();
////        System.out.println("There " + ((numProviders == 1) ? " is " : " are ")
////                + providers.size() + ((numProviders == 1) ? " provider." : " providers. "));
//        if (providers.isEmpty()) {
//            throw new RuntimeException("No service providers found!");
//        }
//        if (theGame == null) {
//            throw new RuntimeException("No suitable service provider found!");
//        }
//        
//        //theCountry = (providers.get(1)); // Careful! Hard-coded! (index 0 so get first one)                       
//            theGame.populate( homeTeam,  awayTeam,  dateTime);
//        return theGame;
//        
//    }
    
    //Iterable<IGameProvider> iter;
    
    
    
    static GameProvider theProvider;
    static String theGameType = "";
    
        public static GameProvider getProvider(String type) {


        // TODO Another possibly better approach would be to make GameProvider itself a singleton
        if ((theProvider!=null) && theGameType.equals(type)) return theProvider; // So the same provider is returned 

        theGameType = type;
        Iterable<GameProvider> iter = ServiceLoader.load(GameProvider.class);

        
        for (GameProvider tempProvider: iter){
            //System.out.println(tempProvider.getClass() + " : " + tempProvider.hashCode());
            if (tempProvider.getType().equalsIgnoreCase(type)){
                theProvider = tempProvider;
                break;
            }
        }
 
        
        if (theProvider == null) {
            throw new RuntimeException("No suitable service provider found!");
        }

        return theProvider;
        
    }


}
