/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package customAnnotation;

public class NewMain {

    public static void main(String[] args) {

        System.out.println(
                 Gen1List.class.getAnnotation(ClassPreamble.class)      +"\n\n" 
                +Gen2List.class.getAnnotation(ClassPreamble.class)      +"\n\n" 
                +Gen2List.class.getAnnotation(ClassPreamble.class).author());
        
/*
        //Example for printing repeatable annotations
        System.out.println(
                 Gen1List.class.getAnnotation(PreamblesContainer.class).value()[0]          +"\n\n" 
                +Gen1List.class.getAnnotation(PreamblesContainer.class).value()[1]          +"\n\n"
                +Gen1List.class.getAnnotationsByType(ClassPreamble.class)[1]                +"\n\n" 
                +Gen1List.class.getAnnotationsByType(ClassPreamble.class)[1].author());
*/
    }
}
