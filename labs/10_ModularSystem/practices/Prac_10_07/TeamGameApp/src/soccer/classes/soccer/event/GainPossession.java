/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package soccer.event;


/**
 *
 * 
 */

// Perhaps break this into Tackle and InterceptPass

public class GainPossession extends SoccerEvent {
    
    public GainPossession(){
        
        super();
        
    }
    
    public String toString() {
        return "* WON possession ";
    }
    
    public SoccerEvent[] getNextEvents() {
        SoccerEvent theEvent[] = { new Pass(), new Dribble(), new Shoot()};
        return theEvent;
    }
    
    public boolean changePlayer() {
        return true;
    }
    
    public boolean changeTeam() {
        return true;
    }
    
}
