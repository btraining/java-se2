package tictactoe;

import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import tictactoe.TicTacToe.eStatus;

/**
 *
 * 
 */
public class TicTacTest {

    public static void main(String[] args) {

        char playerOne = 'X';
        char playerTwo = 'O';

        TicTacToe myTic = new TicTacToe(playerOne, playerTwo);

        int numGames = 0;

        // For testing or demostration, runs games until particular eStatus
        while (myTic.status() != eStatus.PLAYER_WINS) {
        //while (myTic.status() != eStatus.OPPONENT_WINS) { // This should never happen

            long startTime = System.nanoTime();
            numGames++;

            myTic.initialize(); // Initialize (clear the board) for each new game

            // Use various starting points as a way to test
            myTic.either(0,'O');
            myTic.either(1,'X');
            myTic.either(6,'X');
            myTic.either(14,'O');
            myTic.either(15,'O');
            
            // This set requires correct play from engine so tests if it's working correctly.
//            myTic.either(0,'O');
//            myTic.either(1,'X');
//            myTic.either(6,'X');
//            myTic.either(7,'O');
//            myTic.either(12,'O');
//            myTic.either(13,'X');
//            myTic.either(15,'O');

//        This set requires correct play from engine so tests if it's working correctly. 
//            myTic.either(0, 'O');
//            myTic.either(1, 'X');
//            myTic.either(5, 'O');
//            myTic.either(7, 'X');
//            myTic.either(15, 'O');

        // Can it find a fork? Yep.
//        myTic.either(0,'O');
//        myTic.either(12,'O');
//        myTic.either(13,'O');
//        myTic.either(14,'O');
//        myTic.either(4,'X');
//        myTic.either(6,'X');
//        myTic.either(11,'X');
//        myTic.either(15,'X');

            System.out.println("\n=== Starting play ==");
            myTic.showBoard();
            System.out.println("");

            while (myTic.status() == eStatus.IN_PROGRESS) {

                myTic.computerPlayer = playerOne;
                myTic.opponent = playerTwo;
                
                // Move analysis that uses custom Spliterator, TicTacToeSpliterator.
                //calculateMoveCustom(myTic, playerOne);    
                               
                // Move analysis that does not use custom Spliterator. 
                // The first boolean determines if parallel processing enabled.
                // The second boolean determines if pruning is used. Shows how much more  
                // important refining the algorithm is (around factor of 10).
                
                calculateMove(myTic, playerOne, true, true);    
                                                          
                                                          
                
                // Shouldn't play further if game over.
                if (myTic.status() != eStatus.IN_PROGRESS) {
                    break;
                }

                // Random move by computer
                myTic.computer(playerTwo);

            }

            // Game over, show result.
            System.out.println("\n" + myTic.status());
            myTic.showBoard();

            System.out.println("\n*** Game completed ***\n");

            long endTime = System.nanoTime();

            System.out.println("Took " + ((float) endTime - startTime) / 1_000_000_000 + " seconds");

        }

        System.out.println("\n" + numGames + " games played.");

    }
    

    // This version of calculateMove does not use a custom Spliterator.
    // It does the following:
    // * Creates an ArrayList populated with the possible moves from the current position
    // * Creates a Stream based on this List
    // * Maps this Stream to a List of eStatus types (the mapping function uses exhaustive minimax to determine a good move)
    // * Choose the appropriate move as in other calculateMove method.
    // * Make the move (TODO. Poss return the move rather than making it here).
    //
    static void calculateMove(TicTacToe myTic, char computerPlayer, boolean isParallel, boolean pruning) {

        System.out.println("Before beginning calculations: " + myTic.status() + "\n");
        System.out.println("Moves remaining before testing next move: " + (myTic.boardSize * myTic.boardSize - myTic.movesTaken) + "\n");
        System.out.println("\n" + myTic.status());
        myTic.showBoard();
        System.out.println("Before checking move");

        List<TicTacToe> candidateMoves = new ArrayList<>();
        for (int i = 0; i < (myTic.boardSize * myTic.boardSize); i++) {

            // If clause to ensure only valid moves considered. Bit ugly but works. TODO tidy up
            if (myTic.getBoard()[i / myTic.boardSize][i % myTic.boardSize] == '-') {
                TicTacToe tempTic = myTic.createCopy();
                tempTic.getBoard()[i / tempTic.boardSize][i % tempTic.boardSize] = computerPlayer;

                candidateMoves.add(tempTic);
            }

        }

        // Check whether parallel stream required
        Stream<TicTacToe> ticStream = isParallel?candidateMoves.parallelStream():candidateMoves.stream();

        // Note that even though this is a parallel stream, the eStatus list is collected in the 
        // order that corresponds to the original candidate move list. This is as it should be
        // as this is guaranteed by the API.
        //
        List<eStatus> possMoves = ticStream
                .map(n -> MinimaxHelper.getNextMoveValue(n, computerPlayer, pruning))   // computerPlayer can be either player
                .collect(Collectors.toList());

        System.out.println("The list: " + possMoves);

        // Find best move from the list of eStatus values that correspond to the candidate moves
        if (possMoves.indexOf(eStatus.PLAYER_WINS) != -1) {
            myTic.player(myTic.moveIndexToSquareIndex(possMoves.lastIndexOf(eStatus.PLAYER_WINS)), myTic.computerPlayer);
        } else if (possMoves.indexOf(eStatus.TIE_GAME) != -1) {
            myTic.player(myTic.moveIndexToSquareIndex(possMoves.lastIndexOf(eStatus.TIE_GAME)), myTic.computerPlayer);
        } else {
            throw new RuntimeException("This shouldn't happen! Means no possibility of a tie!"); // All moves win for computer!!        
        }


    }
    
    //
    // This version uses a custom Spliterator and also makes the move
    //
    static void calculateMoveCustom(TicTacToe myTic, char computerPlayer) {

        System.out.println("Before beginning calculations: " + myTic.status() + "\n");
        System.out.println("Moves remaining before testing next move: " + (myTic.boardSize * myTic.boardSize - myTic.movesTaken) + "\n");
        System.out.println("\n" + myTic.status());
        myTic.showBoard();
        System.out.println("Before checking move");

        // Create a new Spliterator and then a Stream of type eStatus
        Spliterator s0 = new TicTacToeSpliterator(myTic); //
        Stream<eStatus> ticStream = StreamSupport.stream(s0, true);

        // It seems that in this version of the calculate method (calculateMoveCustom)
        // The standard toList() collect does not retain the order after collection.
        // Most likely this is because the Spliterator is not based on a collection
        // of candidate moves, but rather generates the candidate moves itself.
        //
        Collector moveCollector1 = Collectors.toList();
        //System.out.println(moveCollector1.characteristics()); // It's IDENTITY_FINISH

        Collector<TicTacToe.eStatus, List, List> moveCollector2 = new BestMoveCollector();

        Collector<TicTacToe.eStatus, List, List> moveCollector3
                = Collector.of(
                        () -> new ArrayList(),
                        (theList, move) -> theList.add(move),
                        (list2, list1) -> {
                            list1.addAll(list2);
                            return list1;
                        });

        List<eStatus> possMoves = (List<eStatus>) ticStream // Cast not required for either custom Collector.

                .collect(moveCollector3);

        System.out.println("The list: " + possMoves);

        // This code makes the move, preferring win to draw.
        // Note that using lastIndexOf chooses the last good move on list. indexOf would choose first best move on list
        // where there's more than one WIN or TIE
        // Is there a neater way to do this?
        if (possMoves.indexOf(eStatus.PLAYER_WINS) != -1) {
            myTic.player(myTic.moveIndexToSquareIndex(possMoves.lastIndexOf(eStatus.PLAYER_WINS)), myTic.computerPlayer);
        } else if (possMoves.indexOf(eStatus.TIE_GAME) != -1) {
            myTic.player(myTic.moveIndexToSquareIndex(possMoves.lastIndexOf(eStatus.TIE_GAME)), myTic.computerPlayer);
        } else {
            throw new RuntimeException("This shouldn't happen! Means no possibility of a tie!"); // All moves win for computer!!        
        }

    }

    static int[] createMoveList(TicTacToe tic) {

        int currMove = 0;
        int boardSize = tic.boardSize * tic.boardSize;

        int[] moves = new int[boardSize - tic.movesTaken];

        for (int i = 0; i < boardSize; i++) {
            if (tic.getBoard()[i / tic.boardSize][i % tic.boardSize] == '-') {
                moves[currMove] = i;
                currMove++;
            }

        }

        return moves;
    }

}
