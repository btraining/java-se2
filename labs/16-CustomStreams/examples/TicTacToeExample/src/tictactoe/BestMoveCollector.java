/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tictactoe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

/**
 *
 * 
 */

// Extremely simple Collector
// As with Spliterator, should I genericize the class declaration?
//
// Collector just exists to ensure order is correct when running parallel.
//

public class BestMoveCollector implements Collector<TicTacToe.eStatus, List, List> {

  
    public static  BestMoveCollector toSimpleCollector() {
        return new BestMoveCollector();
    }

    @Override
    public Supplier supplier() {
        return () -> new ArrayList();
    }

    @Override
    public BiConsumer<List, TicTacToe.eStatus> accumulator() {
        return (map, node) -> map.add(node);
        
    }

    // If we don't use a Collector and get the list by (sp)iterating through eStatus values, there's no guarantee
    // order will be correct.
    //
    // Is this guaranteed? I.e. If we combine correctly it'll be eventually in correct order?
    //
    @Override
    public BinaryOperator<List> combiner() {    // This ensures the order correct
        return (buff2, buff1) -> {
            buff1.addAll(buff2);
            return buff1;
        };
    }

    @Override
    // Characteristics.IDENTITY_FINISH ensures this is ignored
    public Function finisher() {
        return Function.identity();
    }

    @Override
    public Set characteristics() {
        return Collections.singleton(Characteristics.IDENTITY_FINISH);
             
        //Set.of(Characteristics.CONCURRENT, Characteristics.IDENTITY_FINISH);
    }
    
    // Setting Characteristics.CONCURRENT breaks things pretty quickly (if enough splits).
    
}
