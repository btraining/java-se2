/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import displaytree.DisplayString;
import game.IGame;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * 
 */

// TODO Perhaps should be instantiated by competition, then mapper stays around
public class JacksonUtil {
    
    static ObjectMapper getMapper() {
        LocalDateDeserializer deserializer = new LocalDateDeserializer();
        LocalDateSerializer serializer = new LocalDateSerializer();
        
        SimpleModule module = new SimpleModule("LongDeserializerModule", new Version(1, 0, 0, null));
        module.addDeserializer(LocalDateTime.class, deserializer);
        module.addSerializer(LocalDateTime.class, serializer);

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(module);

        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.enableDefaultTyping();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        
        return mapper;
    }
    
    public static void saveToJSONFile(String dirName, String filename, IGame[] allGames) {
        
        
        try {
            getMapper().writeValue(new File(dirName, filename), allGames);
        } catch (IOException ioe) {
            System.out.println("trouble writing");
            //ioe.printStackTrace();
            System.out.println(ioe.getMessage());
        }
        
    }
    
    public static IGame[] getGamesFromJSONFile(String dirName, String filename ) {
        
        IGame[] allGames = null;
        try {
            //mapper.writeValue(new File("allGames.json"), allGames);

            //TypeReference<List<List<Game>>> mapType = new TypeReference<List<List<Game>>>() {};
            allGames = getMapper().readValue(new File(dirName, filename), IGame[].class);

        } catch (IOException ioe) {
            System.out.println("Trouble reading");
            //ioe.printStackTrace();
            System.out.println(ioe.getMessage());
        }
        return allGames;
        
    }
    public static String getJSONListOfGames(ArrayList<List> allResults ) {
        
        ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        String theResults = null;

        try {
            //mapper.writeValue(new File("allResults.json"), allResults);
            theResults = mapper.writeValueAsString(allResults);
        } catch (IOException ioe) {
            System.out.println("trouble writing");
            //ioe.printStackTrace();
            System.out.println(ioe.getMessage());
        }

        return theResults;
        
    }
    
        public static String getJSONLeagueDataGrid(DisplayString[][] theGrid ) {
        
        ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        String theResults = null;

        try {
            //mapper.writeValue(new File("allResults.json"), allResults);
            theResults = mapper.writeValueAsString(theGrid);
        } catch (IOException ioe) {
            System.out.println("trouble writing");
            //ioe.printStackTrace();
            System.out.println(ioe.getMessage());
        }

        return theResults;
        
    }
    
}
