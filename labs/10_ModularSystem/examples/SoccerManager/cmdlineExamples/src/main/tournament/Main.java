package tournament;

import displaytree.Display;
import java.util.StringTokenizer;
import tournamentType.Knockout;
import game.ITeam;
import gamefactory.GameFactory;
import tournament.util.PlayerDatabase;
import tournament.util.PlayerDatabaseException;
import tournamentType.League;

public class Main {
    
    static String dirName = "/home/kenny/games";
    
//To create and display a tournament:
//
// - Create teams (randomly from a database of players) or by loading from a file
// - Create a competition based on a style(currently OldLeague or Knockout are available)
//   and pass optionally pass in the list of teams (they can be added later OR are
//   not required if loading a set of completed games from a file.

// - Play all games OR load a set of completed games for that sport 
//      and competition style
// - Call various methods on the competition to display the results of the completed games
    
// TODO - Either create the competition using teams, OR using filename to load completed competition from
    
    
// Current setup is using an IGameProvider to deliver all the concrete implementations of IGame, ITeam, and IPlayer
// getProvider is being called each time needed, but maybe that's okay? TODO, check to see if same object each time
// (see Modular Java book for this).
    
    
    public static void main(String[] args) {
        
        // Create the teams 
        //String teamNames = "Robins, Pelicans, Sparrows, Magpies";
        String teamNames = "Robins, Pelicans, Sparrows, Magpies, Crows, Falcons, Geese, Terns";
        //String teamNames = "Plovers,Ravens,Doves,Robins,Sparrows,Magpies,Crows,Falcons,Pelicans,Geese,Ducks,Eagles,Gulls,Hawks,Owls,Pigeons";
        int teamSize = 5;   // Number of players on each team
        ITeam[] theTeams = null;
        try {
            // TODO in this example team is same regardless of gameType but could be otherwise
            theTeams = createTeams("Basketball", teamNames, teamSize);  
        } catch (PlayerDatabaseException e) {
            e.printStackTrace(System.err);
        }
        
        
        //***** Knockout competition *****//
        // TODO pass in the type of competition
        //Knockout theKnockoutCompetition = new Knockout(theTeams, "Basketball");   // Creates a Knockout competition
  
        // Create and play all the games
        // TODO Needs to return something to indicate success or failure (e.g. are there any teams?)
        //theKnockoutCompetition.createAndPlayAllGames();
        
        //theKnockoutCompetition.saveGamesToJSONFile(dirName, "completedKnockout01.json");

        // OR, just load some completed knockout games from file
        // TODO Needs to return something to indicate success or failure 
        // (e.g. are there any games? are there correct number for knockout?)
        Knockout theKnockoutCompetition = new Knockout(dirName, "completedKnockout01.json");
        
        
        // Display all the games
        // First get a JSON string of the details of all the games as this is 
        // what the ascii_display module requires
        String displayResults = theKnockoutCompetition.getJSONListOfGames();
        
        // Then use ascii_display module to display results
        Display.printLRTree(displayResults);
        
        
        //***** League competition *****//
        
        // Create a new league competition by passing in teams
        League theLeagueCompetition = new League(theTeams, "Soccer");
        
        // Create and play all games using these teams
        theLeagueCompetition.createAndPlayAllGames();
        
        // Save the games in JSON format to a file
        theLeagueCompetition.saveGamesToJSONFile(dirName, "completedLeague03.json");
        
        // Or get the completed games from a file.
        //League 
        theLeagueCompetition = new League(dirName, "completedLeague03.json");

        
        // Get the display grid in JSON format
        String jsonDataGrid = theLeagueCompetition.getJSONLeagueDataGrid(); 
       
        // Call Display type required
        Display.outputTextLeagueGrid(jsonDataGrid);

        
    }
    
            
    
    
    public static ITeam[] createTeams(String gameType, String teamNames, int teamSize) throws tournament.util.PlayerDatabaseException {

        PlayerDatabase playerDB = new PlayerDatabase(gameType);

        StringTokenizer teamNameTokens = new StringTokenizer(teamNames, ",");
        ITeam[] theTeams = new ITeam[teamNameTokens.countTokens()];
        for (int i = 0; i < theTeams.length; i++) {
            theTeams[i] = GameFactory.getProvider(gameType).
                    getTeam(teamNameTokens.nextToken().
                            trim(), playerDB.getTeam(teamSize));
        }

        return theTeams;
    }
    
}
