/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package targetType;

@AnnotationTypeExample
public class ExampleClass {
    //@AnnotationTypeExample
    int exampleField;
    
    //@AnnotationTypeExample
    public int exampleMethod(int x){
        //@AnnotationTypeExample
        int y = 0;
        return x;
    }
}