/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package deprecated;

//@SuppressWarnings("deprecation")
public class MainClass {
    //@SuppressWarnings("deprecation")
    public static void main(String[] args) {
        TestAPI.oldMethod();
    } 
}
