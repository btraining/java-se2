
package narytreeexample;

import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.Stack;

/**
 *
 * 
 */

public class NTreeSpliterator implements Spliterator<Node>  {

    // Default split level. 0 means no split
    // Hard-coded here for demonstration, but could be passed in to the 
    // constructor or calculated from the depth of the tree.
    private static int levelToSplit = 3;    
    
    public final Tree nTree;   // temporarily public for testing

    Stack<NodeInfo> s;                // Stores previous visits to this Node used in tryAdvance

    public Node rootNode = null;       // Node for the spliterator
    Node currNode = null;              // Current leaf node on the traversal

    public NTreeSpliterator(Tree nTree) {
        this(nTree, nTree.getRoot()); 
    }
    public NTreeSpliterator(Tree nTree, int levelToSplit) {

        this(nTree, nTree.getRoot()); 
        NTreeSpliterator.levelToSplit = levelToSplit==1?0:levelToSplit; // 1 not valid level

    }
    
    public NTreeSpliterator(Tree nTree, Node rootSubTree) {
        this.s = new Stack<>();
        s.push(new NodeInfo(rootSubTree, -1));  // -1 no children Nodes visited yet
        
        this.nTree = nTree;

        this.rootNode = rootSubTree;
        this.currNode = this.rootNode;
   
    }
    
    
    
    @Override
    public boolean tryAdvance(Consumer action) {
        
        if (currNode == null) return false;
        
        Node thisNode = getNextNode(this.currNode);
        
        if (thisNode != null) {

            action.accept(thisNode);

            return true;
        }

        return false;
        
    }
  
    private Node getNextNode(Node tempNode) {

        this.currNode = tempNode;

        while (s.peek().childrenChecks != -1) {         // Not first encounter with this Node therefore need to check children

            if (s.peek().childrenChecks < currNode.getChildren().size()) {

                    currNode = currNode.getChildAt(s.peek().childrenChecks);
                    s.push(new NodeInfo(currNode, -1));     // Setting childrenChecks to -1 so will exit loop and return Node

            // return up the stack when all children explored
            } else {
 
                s.pop();
                if (s.empty()) {   // This only happens when we have exhausted all children of the rootNode and therefore popped the rootNode
                    return null;   // Thus the tree has been completely traversed
                }
                currNode = s.peek().node;  // Set currNode to parent
                s.peek().childrenChecks++; // Get ready to check next child
            }

        }
        
        s.peek().childrenChecks++;  // Ensure next time visiting this node start checking children
        return currNode;

    }

    
    @Override
    public Spliterator<Node> trySplit() {   // TODO Could use recursion instead of stack? Though stack gives easy way to check level.
        
        Node currParent = null;
        NTreeSpliterator newSplit = null;
        Stack<NodeInfo> s4 = new Stack<>(); 
        
        s4.push(new NodeInfo(rootNode, 0));     // childrenChecks initialized to zero because we start by traversing down the first child

        Node myNode = rootNode;
        
        //  Logic for finding split point for creating two trees is here
        //  Loop until find a node levelToSplit levels down that has children
        while (s4.size() != levelToSplit || myNode.getChildren().isEmpty()) {     

            if (s4.peek().childrenChecks < myNode.getChildren().size()) {
                    currParent = s4.peek().node;     // Could get parent here for later instead of hard-coded parents?

                    myNode = myNode.getChildAt(s4.peek().childrenChecks);

                    s4.push(new NodeInfo(myNode, 0));

            } else {

                s4.pop();
                if (s4.empty()) {
                    return null;    // If there's no way to split further
                }
                myNode = s4.peek().node;
                s4.peek().childrenChecks++;
            }
        }
        
        // Loop has exited so split point has been found
        newSplit = new NTreeSpliterator(nTree, myNode);  // Create new Spliterator
        currParent.children.remove(myNode);                         // Ensure current Spliterator cannot produce nodes in new Spliterator
        currParent.possLeaf = false;                                // Ensure that the pruning of existing Spliterator doesn't result in extra leaf nodes
        //System.out.println(">>>>> Splitting at: " + myNode);
        return newSplit;

    }
    

    @Override
    public long estimateSize() {

        return nTree.size();
    }

    @Override
    public int characteristics() {
        return 0;
    }
    
}

class NodeInfo {
    
    int childrenChecks;                   // -1 means not yet visited
    Node node = null;                     // 0 means visiting first child node etc.
    NodeInfo (Node n, int childIndexChecked) {
        this.node = n;
        this.childrenChecks = childIndexChecked;
    }
    
}