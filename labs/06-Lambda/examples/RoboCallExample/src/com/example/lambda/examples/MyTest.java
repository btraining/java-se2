package com.example.lambda.examples;

/**
 * 
 */
public interface MyTest<T> {
  public boolean test(T t);
}
