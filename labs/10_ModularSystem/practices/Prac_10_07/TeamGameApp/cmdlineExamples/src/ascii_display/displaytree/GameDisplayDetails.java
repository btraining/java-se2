/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package displaytree;

//import soccer.play.*;

/**
 *
 * 
 */
public class GameDisplayDetails {
    
    private String homeTeam;
    private String awayTeam;
    private String score;
    
    GameDisplayDetails() {}
    
    public GameDisplayDetails(String homeTeam, String awayTeam, String score){
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.score = score;
        
    }

    /**
     * @return the homeTeam
     */
    public String getHomeTeam() {
        return homeTeam;
    }

    /**
     * @return the awayTeam
     */
    public String getAwayTeam() {
        return awayTeam;
    }

    /**
     * @return the score
     */
    public String getScore() {
        return score;
    }
    
    
    
}
