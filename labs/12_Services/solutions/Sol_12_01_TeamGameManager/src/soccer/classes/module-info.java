/* Copyright © 2017 Oracle and/or its affiliates. All rights reserved. */


module soccer {
    requires gameapi;
    requires java.logging;
    opens soccer to jackson.databind;

    provides gameapi.GameProvider with soccer.SoccerProvider;
}
