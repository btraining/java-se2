/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package customAnnotation;


@ClassPreamble(
    author = "Yu Wong",
    reviewers = {"Ben Ng", "Robin Peck"}
)
public class Gen2List extends Gen1List {
    
}
