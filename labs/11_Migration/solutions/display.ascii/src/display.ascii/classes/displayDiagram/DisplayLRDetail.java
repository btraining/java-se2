/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package displayDiagram;

//import soccer.play.*;


/**
 * 
 * Some things will be hard-coded, but eventually can be in a separate class (that cd read from a file)
 *
 * 
 */
public class DisplayLRDetail {
    
    // Should each object store the details of the above row?
    
    /* Typical display box
        ------------------    
        |  Doves  Ducks  |
        |     2 - 1      |
        ------------------
    */
    boolean isFinalRound;
    String homeTeam;
    String awayTeam;
    String winner;
    
    int teamPaddingNumChars = 1;
    int boxWidth = 18 + teamPaddingNumChars*2;
    
    int distBoxToVbar = 3;
    int distVbarToBox = 3;
    // This sets the column position, colPos
    int distToNextRow = boxWidth + distBoxToVbar + distVbarToBox + 1; // +1 is for the Vbar
    
    int verticalPos;

    int boxDepth = 5;  // Height of box. Not dynamic but should be used by calling method?

    String score;
    

    //String paddingChar = "#";   // Default is to make padding visible (NOT being used)

    DisplayLRDetail(GameDisplayDetails currGame, int rowNum, int verticalPos, boolean isFinalRound) { 
        this.homeTeam = currGame.getHomeTeam();
        this.awayTeam = currGame.getAwayTeam();


        this.isFinalRound = isFinalRound;        
        this.score = currGame.getScore();
        
        this.verticalPos = verticalPos;

    }

    
    String getPaddingString(int size, String replaceChar) {
        return new String(new char[size]).replace("\0", replaceChar);
    }
    String getPaddingString(String theString, String replaceChar) {
        return new String(new char[theString.length()]).replace("\0", replaceChar);
    }
    
    private String printTeamRow() {
        
        String teamString = "|" + getPaddingString(teamPaddingNumChars," ") 
                + homeTeam + getPaddingString(teamPaddingNumChars," ") 
                + awayTeam;
        int fillOut = boxWidth - teamString.length() - 1;
        teamString += getPaddingString(fillOut," ");
        return teamString + "|";
    }
    
    private String printScore() {
        
        // TODO 
        // This is from previous code - can be improved a lot
        // Maybe make general as needed several times
        // Also != here but == in other place

        String scoreString = "|    " + score;
        int fillOut = boxWidth - scoreString.length() - 1;
        scoreString += getPaddingString(fillOut," ");
        return scoreString + "|";

    }
    
    void printGameRow(char[][] outputArray, int roundNum, int gameNum, int prevRowPos) {
        
        String theGameRow;
        
        int rowsPrinted = 0;
        int colPos = 0;
        int rowPos;
    
        rowPos = this.verticalPos;
        
        // ** The multiplier here represents the horizontal distance between columns of games
        
        colPos = roundNum * distToNextRow;    // Was 28
    
        
        // top row
        theGameRow = getPaddingString(boxWidth,"-");
        for (int i = 0; i < theGameRow.length(); i ++) {
            outputArray[rowPos + rowsPrinted][i + colPos] = '-';//theGameRow.toCharArray()[i];
        }
        rowsPrinted++;
        
        // teams row
        theGameRow = printTeamRow();
        for (int i = 0; i < theGameRow.length(); i ++) {
            outputArray[rowPos + rowsPrinted][i + colPos] = theGameRow.toCharArray()[i];
        }
        rowsPrinted++;
        
        // Space row plus connection to bar 
        // ternary to not draw last horizontal bar
        theGameRow = "|" + getPaddingString(boxWidth - 2 ," ") + "|" + getPaddingString((!isFinalRound)?distBoxToVbar:0,"-");
        for (int i = 0; i < theGameRow.length(); i ++) {
            outputArray[rowPos + rowsPrinted][i + colPos] = theGameRow.toCharArray()[i];
        }
 
        
        //Connector from vertical bar
        if (roundNum > 0) {
            theGameRow = getPaddingString(distVbarToBox,"-"); // TODO should be based on column boxWidth
            for (int i = 0; i < theGameRow.length(); i++) {
                outputArray[rowPos + rowsPrinted][colPos - i - 1] = theGameRow.toCharArray()[i];
            }
        }
       
        // the downward bar if the gameNum is odd
        if (gameNum%2 != 0) {
            for (int i = 0; i <= this.verticalPos - prevRowPos; i++) {
                
                outputArray[rowPos + rowsPrinted - i][colPos  + boxWidth + distBoxToVbar] = '|';
                //System.out.print("!!" + (rowPos + rowsPrinted) + " ++ " + (i + colPos);
            }
        }
        rowsPrinted++;
        
        // score row
        theGameRow = printScore();
        for (int i = 0; i < theGameRow.length(); i ++) {
            outputArray[rowPos + rowsPrinted][i + colPos] = theGameRow.toCharArray()[i];
        }
        rowsPrinted++;
        
        // bottom row
        theGameRow = getPaddingString(boxWidth,"-");
        for (int i = 0; i < theGameRow.length(); i ++) {
            outputArray[rowPos + rowsPrinted][i + (colPos)] = theGameRow.toCharArray()[i];
        }
        rowsPrinted++;
    
    }
   
}
