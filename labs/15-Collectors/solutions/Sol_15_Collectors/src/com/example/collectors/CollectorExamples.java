package com.example.collectors;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import static java.util.stream.Collectors.averagingDouble;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.flatMapping;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.summingDouble;
import static java.util.stream.Collectors.summingLong;
import static java.util.stream.Collectors.toList;

/**
 *
 *
 */
public class CollectorExamples {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // Practice 15-1
        // Task 2
        List<ComplexSalesTxn> tList = ComplexSalesTxn.createTxnList();

        for (ComplexSalesTxn currTxn : tList) {
            //System.out.println(currTxn.getSummaryStr());
        }

        // Task 3
        //tList.forEach(a -> System.out.println(a.getSummaryStr()));
        // Task 4
//        tList.forEach(a -> System.out.println(a.getBuyerName()
//                .equals("PriceCo") ? a.getSummaryStr() : ""));
//        tList.forEach(a -> System.out.print(a.getBuyerName()
//                .equals("PriceCo")?a.getSummaryStr() + "\n":""));
        // Task 5
//        tList.stream()
//                .filter(f -> f.getBuyerName().equals("PriceCo"))
//                .map(ComplexSalesTxn::getSummaryStr)
//                .forEach(System.out::println);
        // Task 6
        List<String> priceCoSummary = tList.stream()
                .filter(f -> f.getBuyerName().equals("PriceCo"))
                .map(ComplexSalesTxn::getSummaryStr)
                .collect(toList());

        // priceCoSummary.forEach(System.out::println);
        // Practice 15-2
        // Task 1
        Map<Buyer, List<ComplexSalesTxn>> txns
                = tList.stream()
                        .collect(groupingBy(ComplexSalesTxn::getBuyer));
        // System.out.println(txns);

        // Task 2
        Map<Buyer, List<String>> txns2
                = tList.stream()
                        .collect(groupingBy(ComplexSalesTxn::getBuyer,
                                mapping(ComplexSalesTxn::getSummaryStr, toList())));

        //System.out.println(txns2);
        // Task 3
//        txns.forEach((a, b) -> { System.out.println("<< " + a + " >> ");
//                            b.forEach(System.out::print);
//        });
        // Task 3 note
//        tList.stream()
//                .collect(groupingBy(ComplexSalesTxn::getBuyer,
//                    mapping(ComplexSalesTxn::getSummaryStr, toList())))
//                .forEach((a, b) -> { System.out.println("<< " + a + " >> ");
//                            b.forEach(System.out::println);
//        });;
        // Task 4
//        Map<String, List<List<LineItem>>> txnDetails
//                = tList.stream()
//                .collect(groupingBy(ComplexSalesTxn::getSalesPerson, 
//                        mapping(ComplexSalesTxn::getLineItems, toList())));
//        System.out.println(txnDetails);
        // Task 5
        Map<String, List<LineItem>> txnDetails
                = tList.stream()
                        .collect(groupingBy(ComplexSalesTxn::getSalesPerson,
                                flatMapping(t -> t.getLineItems().stream(), toList())));

        //System.out.println(txnDetails);
        // Task 6
        Map<String, Long> txnCount
                = tList.stream()
                        .collect(groupingBy(ComplexSalesTxn::getSalesPerson,
                                flatMapping(t -> t.getLineItems().stream(), counting())));
        //System.out.println(txnCount);

        // Task 7
        Map<String, Double> txnTotal
                = tList.stream()
                        .collect(groupingBy(ComplexSalesTxn::getSalesPerson,
                                flatMapping(t -> t.getLineItems().stream(),
                                        //counting()
                                        summingDouble(s -> s.getQuantity() * s.getUnitPrice())
                                )));

        //System.out.println(txnTotal);
        //Task 8
//        tList.parallelStream()
//                .collect(groupingBy(ComplexSalesTxn::getSalesPerson,
//                        flatMapping(t -> t.getLineItems().stream(),
//                                //counting()
//                                summingDouble(s -> s.getQuantity() * s.getUnitPrice())
//                        )))
//                .entrySet()
//                .parallelStream()
//                .sorted(Comparator.comparing((Entry<String, Double> a) -> a.getValue()).reversed())
//                .forEachOrdered(System.out::println);



            // Task 9
            
            tList.stream()
.collect(groupingBy(ComplexSalesTxn::getCity,
                    groupingBy(ComplexSalesTxn::getSalesPerson,
                    groupingBy(ComplexSalesTxn::getBuyer,
                            mapping(ComplexSalesTxn::getLineItems,
                            toList())))))
                    .forEach((a, b) -> b.forEach((c, d) -> d.forEach((e, f) -> System.out.println(a + " : " + c + " : " + e + " : " + f) )));;


    }

}
