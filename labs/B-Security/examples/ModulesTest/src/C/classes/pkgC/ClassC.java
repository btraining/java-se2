/* Copyright © 2019 Oracle and/or its affiliates. All rights reserved. */

package pkgC;

import pkgA.ClassA;

public class ClassC {
    private ClassA test = new ClassA();
    private int year = test.YEAR;
}
