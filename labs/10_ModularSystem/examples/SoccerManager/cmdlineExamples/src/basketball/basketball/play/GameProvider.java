/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basketball.play;

import game.IGame;
import game.IGameProvider;
import game.IPlayer;
import game.ITeam;
import java.time.LocalDateTime;

/**
 *
 * 
 */
public class GameProvider implements IGameProvider {
    
    public IGame getGame(ITeam homeTeam, ITeam awayTeam, LocalDateTime plusDays) {
        return new Basketball(homeTeam, awayTeam, plusDays);
    }

    public IPlayer getPlayer(String playerName){
        return new Player(playerName);
    }
    
    public ITeam getTeam(String teamName, IPlayer[] players){
        return new Team(teamName, players);
    }
    public String getType() {
        return "Basketball";
    }
}
