/*
 * /* Copyright © 2017 Oracle and/or its affiliates. All rights reserved. */


module main {
    requires java.logging;
    requires competition;
    requires display.ascii;
    
    requires storage;

}
