/* Copyright © 2017 Oracle and/or its affiliates. All rights reserved. */

module Sol_10_05_ModularSystem {
    requires java.logging;
}
