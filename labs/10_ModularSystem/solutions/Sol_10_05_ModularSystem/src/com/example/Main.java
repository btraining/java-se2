/* Copyright © 2017 Oracle and/or its affiliates. All rights reserved. */

package com.example;

import java.util.logging.Logger;

public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());
    
    public static void main(String[] args) {
        LOGGER.info("HelloWorld App says hello!");
    }
}
